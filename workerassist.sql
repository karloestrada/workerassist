-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 08, 2017 at 11:56 PM
-- Server version: 5.6.33
-- PHP Version: 5.6.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `workerassist`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext,
  `comment_author_email` varchar(100) DEFAULT NULL,
  `comment_author_url` varchar(200) DEFAULT NULL,
  `comment_author_IP` varchar(100) DEFAULT NULL,
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) DEFAULT '1',
  `comment_agent` varchar(255) DEFAULT NULL,
  `comment_type` varchar(20) DEFAULT NULL,
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-01-31 07:23:21', '2017-01-31 07:23:21', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `link_image` varchar(255) DEFAULT NULL,
  `link_target` varchar(25) DEFAULT NULL,
  `link_description` varchar(255) DEFAULT NULL,
  `link_visible` varchar(20) DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) DEFAULT NULL,
  `link_notes` mediumtext,
  `link_rss` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) DEFAULT NULL,
  `option_value` longtext,
  `autoload` varchar(20) DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://workerassist.dev', 'yes'),
(2, 'home', 'http://workerassist.dev', 'yes'),
(3, 'blogname', 'Worker Assist', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'karloestrada@yahoo.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:104:{s:12:"resources/?$";s:29:"index.php?post_type=resources";s:42:"resources/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=resources&feed=$matches[1]";s:37:"resources/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=resources&feed=$matches[1]";s:29:"resources/page/([0-9]{1,})/?$";s:47:"index.php?post_type=resources&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:35:"resources/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"resources/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"resources/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"resources/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"resources/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"resources/.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"resources/(.+?)/embed/?$";s:42:"index.php?resources=$matches[1]&embed=true";s:28:"resources/(.+?)/trackback/?$";s:36:"index.php?resources=$matches[1]&tb=1";s:48:"resources/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?resources=$matches[1]&feed=$matches[2]";s:43:"resources/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?resources=$matches[1]&feed=$matches[2]";s:36:"resources/(.+?)/page/?([0-9]{1,})/?$";s:49:"index.php?resources=$matches[1]&paged=$matches[2]";s:43:"resources/(.+?)/comment-page-([0-9]{1,})/?$";s:49:"index.php?resources=$matches[1]&cpage=$matches[2]";s:32:"resources/(.+?)(?:/([0-9]+))?/?$";s:48:"index.php?resources=$matches[1]&page=$matches[2]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:29:"gravityforms/gravityforms.php";i:1;s:34:"advanced-custom-fields-pro/acf.php";i:2;s:27:"backupbuddy/backupbuddy.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'workerassist', 'yes'),
(41, 'stylesheet', 'workerassist', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'cron', 'a:6:{i:1486581801;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1486596638;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1486606338;a:1:{s:17:"gravityforms_cron";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1486632167;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1486635214;a:1:{s:16:"backupbuddy_cron";a:1:{s:32:"b2d6f4df5dfc2b203ff6fc9b49103fdc";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:2:{i:0;s:12:"housekeeping";i:1;a:0:{}}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(105, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1485855582;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(109, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.7.2.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.7.2.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.7.2-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.7.2-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.7.2";s:7:"version";s:5:"4.7.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1486539273;s:15:"version_checked";s:5:"4.7.2";s:12:"translations";a:0:{}}', 'no'),
(114, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1486539282;s:7:"checked";a:1:{s:12:"workerassist";s:5:"1.0.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(115, '_site_transient_timeout_browser_dff177876e22ddc43470e135e8edbe87', '1486452204', 'no'),
(116, '_site_transient_browser_dff177876e22ddc43470e135e8edbe87', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"55.0.2883.95";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'no'),
(117, 'can_compress_scripts', '1', 'no'),
(138, 'WPLANG', '', 'yes'),
(143, 'current_theme', 'Worker Assist', 'yes'),
(144, 'theme_mods_workerassist', 'a:2:{i:0;b:0;s:18:"custom_css_post_id";i:-1;}', 'yes'),
(145, 'theme_switched', '', 'yes'),
(188, '_site_transient_timeout_browser_1c8fdfa3d4bc6cd41af9115e9b4fb110', '1486942027', 'no'),
(189, '_site_transient_browser_1c8fdfa3d4bc6cd41af9115e9b4fb110', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"56.0.2924.76";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'no'),
(208, '_site_transient_timeout_available_translations', '1486348254', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(209, '_site_transient_available_translations', 'a:108:{s:2:"af";a:8:{s:8:"language";s:2:"af";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:38:06";s:12:"english_name";s:9:"Afrikaans";s:11:"native_name";s:9:"Afrikaans";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/af.zip";s:3:"iso";a:2:{i:1;s:2:"af";i:2;s:3:"afr";}s:7:"strings";a:1:{s:8:"continue";s:10:"Gaan voort";}}s:3:"ary";a:8:{s:8:"language";s:3:"ary";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:35";s:12:"english_name";s:15:"Moroccan Arabic";s:11:"native_name";s:31:"العربية المغربية";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ary.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:3;s:3:"ary";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:49:08";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"as";a:8:{s:8:"language";s:2:"as";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-22 18:59:07";s:12:"english_name";s:8:"Assamese";s:11:"native_name";s:21:"অসমীয়া";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/as.zip";s:3:"iso";a:3:{i:1;s:2:"as";i:2;s:3:"asm";i:3;s:3:"asm";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-06 00:09:27";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:3:"azb";a:8:{s:8:"language";s:3:"azb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-12 20:34:31";s:12:"english_name";s:17:"South Azerbaijani";s:11:"native_name";s:29:"گؤنئی آذربایجان";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:3;s:3:"azb";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:3:"bel";a:8:{s:8:"language";s:3:"bel";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-01 08:27:29";s:12:"english_name";s:10:"Belarusian";s:11:"native_name";s:29:"Беларуская мова";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/bel.zip";s:3:"iso";a:2:{i:1;s:2:"be";i:2;s:3:"bel";}s:7:"strings";a:1:{s:8:"continue";s:20:"Працягнуць";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:53:38";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:12:"Напред";}}s:5:"bn_BD";a:8:{s:8:"language";s:5:"bn_BD";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-04 16:58:43";s:12:"english_name";s:7:"Bengali";s:11:"native_name";s:15:"বাংলা";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/bn_BD.zip";s:3:"iso";a:1:{i:1;s:2:"bn";}s:7:"strings";a:1:{s:8:"continue";s:23:"এগিয়ে চল.";}}s:2:"bo";a:8:{s:8:"language";s:2:"bo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-05 09:44:12";s:12:"english_name";s:7:"Tibetan";s:11:"native_name";s:21:"བོད་ཡིག";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/bo.zip";s:3:"iso";a:2:{i:1;s:2:"bo";i:2;s:3:"tib";}s:7:"strings";a:1:{s:8:"continue";s:24:"མུ་མཐུད།";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-04 20:20:28";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:07";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:3:"ceb";a:8:{s:8:"language";s:3:"ceb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-02 17:25:51";s:12:"english_name";s:7:"Cebuano";s:11:"native_name";s:7:"Cebuano";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip";s:3:"iso";a:2:{i:2;s:3:"ceb";i:3;s:3:"ceb";}s:7:"strings";a:1:{s:8:"continue";s:7:"Padayun";}}s:5:"cs_CZ";a:8:{s:8:"language";s:5:"cs_CZ";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-12 08:46:26";s:12:"english_name";s:5:"Czech";s:11:"native_name";s:12:"Čeština‎";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/cs_CZ.zip";s:3:"iso";a:2:{i:1;s:2:"cs";i:2;s:3:"ces";}s:7:"strings";a:1:{s:8:"continue";s:11:"Pokračovat";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:49:29";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:49:25";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:03";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:14:"de_CH_informal";a:8:{s:8:"language";s:14:"de_CH_informal";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:59";s:12:"english_name";s:30:"German (Switzerland, Informal)";s:11:"native_name";s:21:"Deutsch (Schweiz, Du)";s:7:"package";s:73:"https://downloads.wordpress.org/translation/core/4.7.2/de_CH_informal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:12:"de_DE_formal";a:8:{s:8:"language";s:12:"de_DE_formal";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:17";s:12:"english_name";s:15:"German (Formal)";s:11:"native_name";s:13:"Deutsch (Sie)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.7.2/de_DE_formal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:21";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:3:"dzo";a:8:{s:8:"language";s:3:"dzo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-06-29 08:59:03";s:12:"english_name";s:8:"Dzongkha";s:11:"native_name";s:18:"རྫོང་ཁ";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip";s:3:"iso";a:2:{i:1;s:2:"dz";i:2;s:3:"dzo";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 13:41:24";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:51:11";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_NZ";a:8:{s:8:"language";s:5:"en_NZ";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:30";s:12:"english_name";s:21:"English (New Zealand)";s:11:"native_name";s:21:"English (New Zealand)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/en_NZ.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:49:34";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 00:40:28";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_ZA";a:8:{s:8:"language";s:5:"en_ZA";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:53:43";s:12:"english_name";s:22:"English (South Africa)";s:11:"native_name";s:22:"English (South Africa)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/en_ZA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:47:07";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:28";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:41:05";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_GT";a:8:{s:8:"language";s:5:"es_GT";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:37";s:12:"english_name";s:19:"Spanish (Guatemala)";s:11:"native_name";s:21:"Español de Guatemala";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_GT.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CO";a:8:{s:8:"language";s:5:"es_CO";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:37";s:12:"english_name";s:18:"Spanish (Colombia)";s:11:"native_name";s:20:"Español de Colombia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_CO.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_VE";a:8:{s:8:"language";s:5:"es_VE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:53:56";s:12:"english_name";s:19:"Spanish (Venezuela)";s:11:"native_name";s:21:"Español de Venezuela";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_VE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-28 20:09:49";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-09 09:36:22";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_AR";a:8:{s:8:"language";s:5:"es_AR";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:41:31";s:12:"english_name";s:19:"Spanish (Argentina)";s:11:"native_name";s:21:"Español de Argentina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/es_AR.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"et";a:8:{s:8:"language";s:2:"et";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 16:37:11";s:12:"english_name";s:8:"Estonian";s:11:"native_name";s:5:"Eesti";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/et.zip";s:3:"iso";a:2:{i:1;s:2:"et";i:2;s:3:"est";}s:7:"strings";a:1:{s:8:"continue";s:6:"Jätka";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:33";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:24";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:25";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:53:51";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_BE";a:8:{s:8:"language";s:5:"fr_BE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:32";s:12:"english_name";s:16:"French (Belgium)";s:11:"native_name";s:21:"Français de Belgique";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/fr_BE.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_CA";a:8:{s:8:"language";s:5:"fr_CA";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:30";s:12:"english_name";s:15:"French (Canada)";s:11:"native_name";s:19:"Français du Canada";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/fr_CA.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-08-23 17:41:37";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:27";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"gu";a:8:{s:8:"language";s:2:"gu";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:07";s:12:"english_name";s:8:"Gujarati";s:11:"native_name";s:21:"ગુજરાતી";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/gu.zip";s:3:"iso";a:2:{i:1;s:2:"gu";i:2;s:3:"guj";}s:7:"strings";a:1:{s:8:"continue";s:31:"ચાલુ રાખવું";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-05 00:59:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip";s:3:"iso";a:1:{i:3;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:45";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:8:"המשך";}}s:5:"hi_IN";a:8:{s:8:"language";s:5:"hi_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:41";s:12:"english_name";s:5:"Hindi";s:11:"native_name";s:18:"हिन्दी";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/hi_IN.zip";s:3:"iso";a:2:{i:1;s:2:"hi";i:2;s:3:"hin";}s:7:"strings";a:1:{s:8:"continue";s:12:"जारी";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:13";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:39";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:10:"Folytatás";}}s:2:"hy";a:8:{s:8:"language";s:2:"hy";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-03 16:21:10";s:12:"english_name";s:8:"Armenian";s:11:"native_name";s:14:"Հայերեն";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip";s:3:"iso";a:2:{i:1;s:2:"hy";i:2;s:3:"hye";}s:7:"strings";a:1:{s:8:"continue";s:20:"Շարունակել";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:27";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 15:18:24";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:22";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:17";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ka_GE";a:8:{s:8:"language";s:5:"ka_GE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:24";s:12:"english_name";s:8:"Georgian";s:11:"native_name";s:21:"ქართული";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ka_GE.zip";s:3:"iso";a:2:{i:1;s:2:"ka";i:2;s:3:"kat";}s:7:"strings";a:1:{s:8:"continue";s:30:"გაგრძელება";}}s:3:"kab";a:8:{s:8:"language";s:3:"kab";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:13";s:12:"english_name";s:6:"Kabyle";s:11:"native_name";s:9:"Taqbaylit";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/kab.zip";s:3:"iso";a:2:{i:2;s:3:"kab";i:3;s:3:"kab";}s:7:"strings";a:1:{s:8:"continue";s:6:"Kemmel";}}s:2:"km";a:8:{s:8:"language";s:2:"km";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-07 02:07:59";s:12:"english_name";s:5:"Khmer";s:11:"native_name";s:27:"ភាសាខ្មែរ";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/km.zip";s:3:"iso";a:2:{i:1;s:2:"km";i:2;s:3:"khm";}s:7:"strings";a:1:{s:8:"continue";s:12:"បន្ត";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:39:53";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:3:"ckb";a:8:{s:8:"language";s:3:"ckb";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:25";s:12:"english_name";s:16:"Kurdish (Sorani)";s:11:"native_name";s:13:"كوردی‎";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/ckb.zip";s:3:"iso";a:2:{i:1;s:2:"ku";i:3;s:3:"ckb";}s:7:"strings";a:1:{s:8:"continue";s:30:"به‌رده‌وام به‌";}}s:2:"lo";a:8:{s:8:"language";s:2:"lo";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-12 09:59:23";s:12:"english_name";s:3:"Lao";s:11:"native_name";s:21:"ພາສາລາວ";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip";s:3:"iso";a:2:{i:1;s:2:"lo";i:2;s:3:"lao";}s:7:"strings";a:1:{s:8:"continue";s:18:"ຕໍ່​ໄປ";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:34";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:2:"lv";a:8:{s:8:"language";s:2:"lv";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 07:09:15";s:12:"english_name";s:7:"Latvian";s:11:"native_name";s:16:"Latviešu valoda";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/lv.zip";s:3:"iso";a:2:{i:1;s:2:"lv";i:2;s:3:"lav";}s:7:"strings";a:1:{s:8:"continue";s:9:"Turpināt";}}s:5:"mk_MK";a:8:{s:8:"language";s:5:"mk_MK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:41";s:12:"english_name";s:10:"Macedonian";s:11:"native_name";s:31:"Македонски јазик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/mk_MK.zip";s:3:"iso";a:2:{i:1;s:2:"mk";i:2;s:3:"mkd";}s:7:"strings";a:1:{s:8:"continue";s:16:"Продолжи";}}s:5:"ml_IN";a:8:{s:8:"language";s:5:"ml_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 03:43:32";s:12:"english_name";s:9:"Malayalam";s:11:"native_name";s:18:"മലയാളം";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip";s:3:"iso";a:2:{i:1;s:2:"ml";i:2;s:3:"mal";}s:7:"strings";a:1:{s:8:"continue";s:18:"തുടരുക";}}s:2:"mn";a:8:{s:8:"language";s:2:"mn";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-12 07:29:35";s:12:"english_name";s:9:"Mongolian";s:11:"native_name";s:12:"Монгол";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip";s:3:"iso";a:2:{i:1;s:2:"mn";i:2;s:3:"mon";}s:7:"strings";a:1:{s:8:"continue";s:24:"Үргэлжлүүлэх";}}s:2:"mr";a:8:{s:8:"language";s:2:"mr";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:37";s:12:"english_name";s:7:"Marathi";s:11:"native_name";s:15:"मराठी";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/mr.zip";s:3:"iso";a:2:{i:1;s:2:"mr";i:2;s:3:"mar";}s:7:"strings";a:1:{s:8:"continue";s:25:"सुरु ठेवा";}}s:5:"ms_MY";a:8:{s:8:"language";s:5:"ms_MY";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:49";s:12:"english_name";s:5:"Malay";s:11:"native_name";s:13:"Bahasa Melayu";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ms_MY.zip";s:3:"iso";a:2:{i:1;s:2:"ms";i:2;s:3:"msa";}s:7:"strings";a:1:{s:8:"continue";s:8:"Teruskan";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:6:"4.1.14";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.14/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ဆောင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:31";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"ne_NP";a:8:{s:8:"language";s:5:"ne_NP";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:31";s:12:"english_name";s:6:"Nepali";s:11:"native_name";s:18:"नेपाली";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ne_NP.zip";s:3:"iso";a:2:{i:1;s:2:"ne";i:2;s:3:"nep";}s:7:"strings";a:1:{s:8:"continue";s:43:"जारी राख्नुहोस्";}}s:5:"nl_BE";a:8:{s:8:"language";s:5:"nl_BE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:49:13";s:12:"english_name";s:15:"Dutch (Belgium)";s:11:"native_name";s:20:"Nederlands (België)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/nl_BE.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:48";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:12:"nl_NL_formal";a:8:{s:8:"language";s:12:"nl_NL_formal";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-10-14 13:24:10";s:12:"english_name";s:14:"Dutch (Formal)";s:11:"native_name";s:20:"Nederlands (Formeel)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.7.2/nl_NL_formal.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:57";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-02 13:47:38";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pa_IN";a:8:{s:8:"language";s:5:"pa_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-16 05:19:43";s:12:"english_name";s:7:"Punjabi";s:11:"native_name";s:18:"ਪੰਜਾਬੀ";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip";s:3:"iso";a:2:{i:1;s:2:"pa";i:2;s:3:"pan";}s:7:"strings";a:1:{s:8:"continue";s:25:"ਜਾਰੀ ਰੱਖੋ";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:18";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:6:"4.1.14";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.14/ps.zip";s:3:"iso";a:2:{i:1;s:2:"ps";i:2;s:3:"pus";}s:7:"strings";a:1:{s:8:"continue";s:19:"دوام ورکړه";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:15";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:53";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:3:"rhg";a:8:{s:8:"language";s:3:"rhg";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-16 13:03:18";s:12:"english_name";s:8:"Rohingya";s:11:"native_name";s:8:"Ruáinga";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip";s:3:"iso";a:1:{i:3;s:3:"rhg";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:42:11";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:09:16";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:3:"sah";a:8:{s:8:"language";s:3:"sah";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-21 02:06:41";s:12:"english_name";s:5:"Sakha";s:11:"native_name";s:14:"Сахалыы";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip";s:3:"iso";a:2:{i:2;s:3:"sah";i:3;s:3:"sah";}s:7:"strings";a:1:{s:8:"continue";s:12:"Салҕаа";}}s:5:"si_LK";a:8:{s:8:"language";s:5:"si_LK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-12 06:00:52";s:12:"english_name";s:7:"Sinhala";s:11:"native_name";s:15:"සිංහල";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip";s:3:"iso";a:2:{i:1;s:2:"si";i:2;s:3:"sin";}s:7:"strings";a:1:{s:8:"continue";s:44:"දිගටම කරගෙන යන්න";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:41:00";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:49:04";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:8:"Nadaljuj";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:49";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:41:03";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:40:55";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:3:"szl";a:8:{s:8:"language";s:3:"szl";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-09-24 19:58:14";s:12:"english_name";s:8:"Silesian";s:11:"native_name";s:17:"Ślōnskŏ gŏdka";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip";s:3:"iso";a:1:{i:3;s:3:"szl";}s:7:"strings";a:1:{s:8:"continue";s:13:"Kōntynuować";}}s:5:"ta_IN";a:8:{s:8:"language";s:5:"ta_IN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 03:22:47";s:12:"english_name";s:5:"Tamil";s:11:"native_name";s:15:"தமிழ்";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip";s:3:"iso";a:2:{i:1;s:2:"ta";i:2;s:3:"tam";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:2:"te";a:8:{s:8:"language";s:2:"te";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:47:39";s:12:"english_name";s:6:"Telugu";s:11:"native_name";s:18:"తెలుగు";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/te.zip";s:3:"iso";a:2:{i:1;s:2:"te";i:2;s:3:"tel";}s:7:"strings";a:1:{s:8:"continue";s:30:"కొనసాగించు";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:43";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:2:"tl";a:8:{s:8:"language";s:2:"tl";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-30 02:38:08";s:12:"english_name";s:7:"Tagalog";s:11:"native_name";s:7:"Tagalog";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip";s:3:"iso";a:2:{i:1;s:2:"tl";i:2;s:3:"tgl";}s:7:"strings";a:1:{s:8:"continue";s:10:"Magpatuloy";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:59";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"tt_RU";a:8:{s:8:"language";s:5:"tt_RU";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-11-20 20:20:50";s:12:"english_name";s:5:"Tatar";s:11:"native_name";s:19:"Татар теле";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip";s:3:"iso";a:2:{i:1;s:2:"tt";i:2;s:3:"tat";}s:7:"strings";a:1:{s:8:"continue";s:17:"дәвам итү";}}s:3:"tah";a:8:{s:8:"language";s:3:"tah";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-03-06 18:39:39";s:12:"english_name";s:8:"Tahitian";s:11:"native_name";s:10:"Reo Tahiti";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip";s:3:"iso";a:3:{i:1;s:2:"ty";i:2;s:3:"tah";i:3;s:3:"tah";}s:7:"strings";a:1:{s:8:"continue";s:0:"";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2016-12-05 09:23:39";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-27 19:24:08";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:2:"ur";a:8:{s:8:"language";s:2:"ur";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:36:52";s:12:"english_name";s:4:"Urdu";s:11:"native_name";s:8:"اردو";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/ur.zip";s:3:"iso";a:2:{i:1;s:2:"ur";i:2;s:3:"urd";}s:7:"strings";a:1:{s:8:"continue";s:19:"جاری رکھیں";}}s:5:"uz_UZ";a:8:{s:8:"language";s:5:"uz_UZ";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:02";s:12:"english_name";s:5:"Uzbek";s:11:"native_name";s:11:"O‘zbekcha";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/uz_UZ.zip";s:3:"iso";a:2:{i:1;s:2:"uz";i:2;s:3:"uzb";}s:7:"strings";a:1:{s:8:"continue";s:11:"Davom etish";}}s:2:"vi";a:8:{s:8:"language";s:2:"vi";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:48:35";s:12:"english_name";s:10:"Vietnamese";s:11:"native_name";s:14:"Tiếng Việt";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.7.2/vi.zip";s:3:"iso";a:2:{i:1;s:2:"vi";i:2;s:3:"vie";}s:7:"strings";a:1:{s:8:"continue";s:12:"Tiếp tục";}}s:5:"zh_HK";a:8:{s:8:"language";s:5:"zh_HK";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:55:14";s:12:"english_name";s:19:"Chinese (Hong Kong)";s:11:"native_name";s:16:"香港中文版	";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/zh_HK.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:54:45";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.7.2";s:7:"updated";s:19:"2017-01-26 15:55:10";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.7.2/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}}', 'no'),
(217, 'recently_activated', 'a:0:{}', 'yes'),
(221, 'acf_version', '5.5.5', 'yes'),
(234, '_site_transient_timeout_browser_cc5d8203ce9b96784a0bea9c5a67de54', '1486978852', 'no'),
(235, '_site_transient_browser_cc5d8203ce9b96784a0bea9c5a67de54', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"56.0.2924.87";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'no'),
(236, 'options_show_next_step_graphic', '1', 'no'),
(237, '_options_show_next_step_graphic', 'field_58983d871b07c', 'no'),
(238, 'options_enable_homepage_slider', '1', 'no'),
(239, '_options_enable_homepage_slider', 'field_58983db11b07d', 'no'),
(240, 'options_enable__resource_slider', '1', 'no'),
(241, '_options_enable__resource_slider', 'field_58983dc91b07e', 'no'),
(242, 'options_enable_news_slider', '1', 'no'),
(243, '_options_enable_news_slider', 'field_58983de01b07f', 'no'),
(245, 'options_next_step_image', '31', 'no'),
(246, '_options_next_step_image', 'field_589846e602cd7', 'no'),
(247, 'options_homepage_slider_0_homepage_image', '32', 'no'),
(248, '_options_homepage_slider_0_homepage_image', 'field_58983e151b081', 'no'),
(249, 'options_homepage_slider', '2', 'no'),
(250, '_options_homepage_slider', 'field_58983df81b080', 'no'),
(254, 'options_whatwedo_intro_title', 'FREE, INDEPENDENT AND CONFIDENTIAL ADVICE FOR INJURED WORKERS', 'no'),
(255, '_options_whatwedo_intro_title', 'field_589858876e1c0', 'no'),
(256, 'options_whatwedo_intro_text', 'If you’ve been injured at work, the process can be confusing and confronting. Worker Assist Tasmania Inc. can help by providing free and independent advice about workers compensation, rehabilitation and return-to-work. If you aren’t sure what to do, don’t know your legal rights, are feeling frustrated or have other barriers, we can give you the information, action and support you need.\r\n\r\nTake the next step and call Worker Assist Tasmania Inc. on 1300 027 747 or <a class="scroll-contact" href="#contact">request a call</a> by leaving your details. ', 'no'),
(257, '_options_whatwedo_intro_text', 'field_589858ab48a16', 'no'),
(258, 'options_whatwedo_services', '3', 'no'),
(259, '_options_whatwedo_services', 'field_589858eda289b', 'no'),
(260, 'options_whatwedo_services_0_service_image', '46', 'no'),
(261, '_options_whatwedo_services_0_service_image', 'field_58985a5aa289c', 'no'),
(262, 'options_whatwedo_services_0_service_title', 'WORKERS COMPENSATION', 'no'),
(263, '_options_whatwedo_services_0_service_title', 'field_58985a7ba289d', 'no'),
(264, 'options_whatwedo_services_0_service_description', 'If you’ve been injured at work and \r\nneed medical treatment or time off, you might want to claim for workers compensation. We can show you how  to make your claim, talk to you about what to do if your claim is disputed and put you in touch with private law firms \r\nfor complex issues. ', 'no'),
(265, '_options_whatwedo_services_0_service_description', 'field_58985a85a289e', 'no'),
(266, 'options_whatwedo_services_1_service_image', '45', 'no'),
(267, '_options_whatwedo_services_1_service_image', 'field_58985a5aa289c', 'no'),
(268, 'options_whatwedo_services_1_service_title', 'REHABILITATION', 'no'),
(269, '_options_whatwedo_services_1_service_title', 'field_58985a7ba289d', 'no'),
(270, 'options_whatwedo_services_1_service_description', 'After an injury you might need help \r\nto make sure you’re ready to go back  to your job. We can work with you to  find tasks you are able to do in the workplace and put you in touch with other services you might need for your recovery, including physiotherapy and psychology.', 'no'),
(271, '_options_whatwedo_services_1_service_description', 'field_58985a85a289e', 'no'),
(272, 'options_whatwedo_services_2_service_image', '44', 'no'),
(273, '_options_whatwedo_services_2_service_image', 'field_58985a5aa289c', 'no'),
(274, 'options_whatwedo_services_2_service_title', 'RETURN-TO-WORK', 'no'),
(275, '_options_whatwedo_services_2_service_title', 'field_58985a7ba289d', 'no'),
(276, 'options_whatwedo_services_2_service_description', 'Getting back to work helps with your long-term health and can even help you get a better outcome from the compensation process. We can explain your rights and responsibilities about returning to work, as well as giving you information about return-to-work and injury management plans.', 'no'),
(277, '_options_whatwedo_services_2_service_description', 'field_58985a85a289e', 'no'),
(312, 'options_links_0_link_title', 'Asbestos Compensation Scheme, WorkSafe Tasmania ', 'no'),
(313, '_options_links_0_link_title', 'field_58990f7f09b0e', 'no'),
(314, 'options_links_0_link_url', '#', 'no'),
(315, '_options_links_0_link_url', 'field_58990f9509b0f', 'no'),
(316, 'options_links_1_link_title', 'Safe Work Australia ', 'no'),
(317, '_options_links_1_link_title', 'field_58990f7f09b0e', 'no'),
(318, 'options_links_1_link_url', '#', 'no'),
(319, '_options_links_1_link_url', 'field_58990f9509b0f', 'no'),
(320, 'options_links_2_link_title', 'Tasmanian Workers Compensation Legislation', 'no'),
(321, '_options_links_2_link_title', 'field_58990f7f09b0e', 'no'),
(322, 'options_links_2_link_url', '#', 'no'),
(323, '_options_links_2_link_url', 'field_58990f9509b0f', 'no'),
(324, 'options_links_3_link_title', 'WorkCover Tasmania', 'no'),
(325, '_options_links_3_link_title', 'field_58990f7f09b0e', 'no'),
(326, 'options_links_3_link_url', '#', 'no'),
(327, '_options_links_3_link_url', 'field_58990f9509b0f', 'no'),
(328, 'options_links_4_link_title', 'Workers Rehabilitation and Compensation Tribunal of Tasmania  ', 'no'),
(329, '_options_links_4_link_title', 'field_58990f7f09b0e', 'no'),
(330, 'options_links_4_link_url', '#', 'no'),
(331, '_options_links_4_link_url', 'field_58990f9509b0f', 'no'),
(332, 'options_links_5_link_title', 'WorkSafe Tasmania', 'no'),
(333, '_options_links_5_link_title', 'field_58990f7f09b0e', 'no'),
(334, 'options_links_5_link_url', '#', 'no'),
(335, '_options_links_5_link_url', 'field_58990f9509b0f', 'no'),
(336, 'options_links_6_link_title', 'Unions Tasmania', 'no'),
(337, '_options_links_6_link_title', 'field_58990f7f09b0e', 'no'),
(338, 'options_links_6_link_url', '#', 'no'),
(339, '_options_links_6_link_url', 'field_58990f9509b0f', 'no'),
(340, 'options_links', '7', 'no'),
(341, '_options_links', 'field_58990f7009b0d', 'no'),
(343, 'options_homepage_slider_1_homepage_image', '70', 'no'),
(344, '_options_homepage_slider_1_homepage_image', 'field_58983e151b081', 'no'),
(346, 'options_resource_banner_image', '70', 'no'),
(347, '_options_resource_banner_image', 'field_58984557e4e65', 'no'),
(348, 'options_news_banner_image', '71', 'no'),
(349, '_options_news_banner_image', 'field_589845e7bae59', 'no'),
(354, 'widget_gform_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(355, 'gravityformsaddon_gravityformswebapi_version', '1.0', 'yes'),
(356, 'gform_pending_installation', '', 'yes'),
(357, 'gform_enable_background_updates', '1', 'yes'),
(358, 'rg_form_version', '2.1.2', 'yes'),
(386, 'rg_gforms_key', '4ea8a0cc54a27154034436455bdbc646', 'yes'),
(387, 'rg_gforms_enable_akismet', '1', 'yes'),
(388, 'rg_gforms_currency', 'AUD', 'yes'),
(389, 'gform_enable_toolbar_menu', '1', 'yes'),
(396, 'category_children', 'a:0:{}', 'yes'),
(429, 'gform_email_count', '16', 'yes'),
(436, 'gform_upload_page_slug', '505cd16170241f2', 'yes'),
(439, '_transient_timeout_GFCache_9bac542fd9b6cd8adfaa9aeaa45b8680', '1486454765', 'no'),
(440, '_transient_GFCache_9bac542fd9b6cd8adfaa9aeaa45b8680', '1', 'no'),
(445, '_transient_timeout_GFCache_337d8b06fcf75830fece233474c8148f', '1486452002', 'no'),
(446, '_transient_GFCache_337d8b06fcf75830fece233474c8148f', '1', 'no'),
(453, '_transient_timeout_GFCache_ae46db1e87560d55c30d58d1afdbc21e', '1486452157', 'no'),
(454, '_transient_GFCache_ae46db1e87560d55c30d58d1afdbc21e', '1', 'no'),
(455, '_transient_timeout_GFCache_e793c79d872f26d87799e73cdedea8b8', '1486452187', 'no'),
(456, '_transient_GFCache_e793c79d872f26d87799e73cdedea8b8', '1', 'no'),
(457, '_transient_timeout_GFCache_250258492526c2df434f82a2b1b6994d', '1486452182', 'no'),
(458, '_transient_GFCache_250258492526c2df434f82a2b1b6994d', '1', 'no'),
(459, '_transient_timeout_GFCache_dbfba28dd71a986716c38cf23b6c3123', '1486452185', 'no'),
(460, '_transient_GFCache_dbfba28dd71a986716c38cf23b6c3123', '1', 'no'),
(473, '_transient_timeout_GFCache_01ae34cf5d1f1dd177224e0d6a24c645', '1486455335', 'no'),
(474, '_transient_GFCache_01ae34cf5d1f1dd177224e0d6a24c645', '1', 'no'),
(475, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1486466000', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(476, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:100:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"6160";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"3778";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"3777";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"3305";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"2885";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"2583";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"2255";}s:11:"woocommerce";a:3:{s:4:"name";s:11:"woocommerce";s:4:"slug";s:11:"woocommerce";s:5:"count";s:4:"2175";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"2161";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"2138";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"2104";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"2066";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1998";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1964";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:4:"1791";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:4:"1688";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:4:"1667";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:4:"1491";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:4:"1405";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:4:"1312";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:4:"1304";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:4:"1172";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:4:"1145";}s:9:"ecommerce";a:3:{s:4:"name";s:9:"ecommerce";s:4:"slug";s:9:"ecommerce";s:5:"count";s:4:"1081";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:4:"1035";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:4:"1034";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"991";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"987";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"979";}s:10:"responsive";a:3:{s:4:"name";s:10:"responsive";s:4:"slug";s:10:"responsive";s:5:"count";s:3:"958";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"941";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"920";}s:8:"security";a:3:{s:4:"name";s:8:"security";s:4:"slug";s:8:"security";s:5:"count";s:3:"874";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"872";}s:10:"e-commerce";a:3:{s:4:"name";s:10:"e-commerce";s:4:"slug";s:10:"e-commerce";s:5:"count";s:3:"871";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"839";}s:5:"share";a:3:{s:4:"name";s:5:"Share";s:4:"slug";s:5:"share";s:5:"count";s:3:"799";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"794";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"792";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"776";}s:6:"slider";a:3:{s:4:"name";s:6:"slider";s:4:"slug";s:6:"slider";s:5:"count";s:3:"769";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"769";}s:9:"analytics";a:3:{s:4:"name";s:9:"analytics";s:4:"slug";s:9:"analytics";s:5:"count";s:3:"766";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"758";}s:3:"css";a:3:{s:4:"name";s:3:"CSS";s:4:"slug";s:3:"css";s:5:"count";s:3:"754";}s:4:"form";a:3:{s:4:"name";s:4:"form";s:4:"slug";s:4:"form";s:5:"count";s:3:"745";}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";s:3:"742";}s:5:"embed";a:3:{s:4:"name";s:5:"embed";s:4:"slug";s:5:"embed";s:5:"count";s:3:"736";}s:6:"search";a:3:{s:4:"name";s:6:"search";s:4:"slug";s:6:"search";s:5:"count";s:3:"736";}s:6:"custom";a:3:{s:4:"name";s:6:"custom";s:4:"slug";s:6:"custom";s:5:"count";s:3:"727";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"714";}s:9:"slideshow";a:3:{s:4:"name";s:9:"slideshow";s:4:"slug";s:9:"slideshow";s:5:"count";s:3:"660";}s:4:"menu";a:3:{s:4:"name";s:4:"menu";s:4:"slug";s:4:"menu";s:5:"count";s:3:"659";}s:6:"button";a:3:{s:4:"name";s:6:"button";s:4:"slug";s:6:"button";s:5:"count";s:3:"653";}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";s:3:"648";}s:5:"theme";a:3:{s:4:"name";s:5:"theme";s:4:"slug";s:5:"theme";s:5:"count";s:3:"638";}s:9:"dashboard";a:3:{s:4:"name";s:9:"dashboard";s:4:"slug";s:9:"dashboard";s:5:"count";s:3:"633";}s:6:"mobile";a:3:{s:4:"name";s:6:"mobile";s:4:"slug";s:6:"mobile";s:5:"count";s:3:"626";}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";s:3:"619";}s:4:"tags";a:3:{s:4:"name";s:4:"tags";s:4:"slug";s:4:"tags";s:5:"count";s:3:"617";}s:10:"categories";a:3:{s:4:"name";s:10:"categories";s:4:"slug";s:10:"categories";s:5:"count";s:3:"605";}s:10:"statistics";a:3:{s:4:"name";s:10:"statistics";s:4:"slug";s:10:"statistics";s:5:"count";s:3:"603";}s:4:"user";a:3:{s:4:"name";s:4:"user";s:4:"slug";s:4:"user";s:5:"count";s:3:"597";}s:6:"editor";a:3:{s:4:"name";s:6:"editor";s:4:"slug";s:6:"editor";s:5:"count";s:3:"595";}s:3:"ads";a:3:{s:4:"name";s:3:"ads";s:4:"slug";s:3:"ads";s:5:"count";s:3:"589";}s:12:"social-media";a:3:{s:4:"name";s:12:"social media";s:4:"slug";s:12:"social-media";s:5:"count";s:3:"584";}s:5:"users";a:3:{s:4:"name";s:5:"users";s:4:"slug";s:5:"users";s:5:"count";s:3:"570";}s:4:"list";a:3:{s:4:"name";s:4:"list";s:4:"slug";s:4:"list";s:5:"count";s:3:"563";}s:12:"contact-form";a:3:{s:4:"name";s:12:"contact form";s:4:"slug";s:12:"contact-form";s:5:"count";s:3:"555";}s:6:"simple";a:3:{s:4:"name";s:6:"simple";s:4:"slug";s:6:"simple";s:5:"count";s:3:"551";}s:9:"affiliate";a:3:{s:4:"name";s:9:"affiliate";s:4:"slug";s:9:"affiliate";s:5:"count";s:3:"550";}s:7:"plugins";a:3:{s:4:"name";s:7:"plugins";s:4:"slug";s:7:"plugins";s:5:"count";s:3:"543";}s:9:"multisite";a:3:{s:4:"name";s:9:"multisite";s:4:"slug";s:9:"multisite";s:5:"count";s:3:"538";}s:9:"marketing";a:3:{s:4:"name";s:9:"marketing";s:4:"slug";s:9:"marketing";s:5:"count";s:3:"537";}s:4:"shop";a:3:{s:4:"name";s:4:"shop";s:4:"slug";s:4:"shop";s:5:"count";s:3:"534";}s:7:"picture";a:3:{s:4:"name";s:7:"picture";s:4:"slug";s:7:"picture";s:5:"count";s:3:"527";}s:3:"api";a:3:{s:4:"name";s:3:"api";s:4:"slug";s:3:"api";s:5:"count";s:3:"521";}s:7:"contact";a:3:{s:4:"name";s:7:"contact";s:4:"slug";s:7:"contact";s:5:"count";s:3:"511";}s:3:"url";a:3:{s:4:"name";s:3:"url";s:4:"slug";s:3:"url";s:5:"count";s:3:"494";}s:10:"newsletter";a:3:{s:4:"name";s:10:"newsletter";s:4:"slug";s:10:"newsletter";s:5:"count";s:3:"477";}s:10:"navigation";a:3:{s:4:"name";s:10:"navigation";s:4:"slug";s:10:"navigation";s:5:"count";s:3:"473";}s:6:"events";a:3:{s:4:"name";s:6:"events";s:4:"slug";s:6:"events";s:5:"count";s:3:"471";}s:4:"html";a:3:{s:4:"name";s:4:"html";s:4:"slug";s:4:"html";s:5:"count";s:3:"469";}s:8:"pictures";a:3:{s:4:"name";s:8:"pictures";s:4:"slug";s:8:"pictures";s:5:"count";s:3:"468";}s:8:"tracking";a:3:{s:4:"name";s:8:"tracking";s:4:"slug";s:8:"tracking";s:5:"count";s:3:"456";}s:10:"shortcodes";a:3:{s:4:"name";s:10:"shortcodes";s:4:"slug";s:10:"shortcodes";s:5:"count";s:3:"454";}s:4:"meta";a:3:{s:4:"name";s:4:"meta";s:4:"slug";s:4:"meta";s:5:"count";s:3:"446";}s:8:"calendar";a:3:{s:4:"name";s:8:"calendar";s:4:"slug";s:8:"calendar";s:5:"count";s:3:"445";}s:11:"advertising";a:3:{s:4:"name";s:11:"advertising";s:4:"slug";s:11:"advertising";s:5:"count";s:3:"439";}s:8:"lightbox";a:3:{s:4:"name";s:8:"lightbox";s:4:"slug";s:8:"lightbox";s:5:"count";s:3:"436";}s:3:"tag";a:3:{s:4:"name";s:3:"tag";s:4:"slug";s:3:"tag";s:5:"count";s:3:"435";}s:6:"paypal";a:3:{s:4:"name";s:6:"paypal";s:4:"slug";s:6:"paypal";s:5:"count";s:3:"429";}s:9:"thumbnail";a:3:{s:4:"name";s:9:"thumbnail";s:4:"slug";s:9:"thumbnail";s:5:"count";s:3:"428";}s:6:"upload";a:3:{s:4:"name";s:6:"upload";s:4:"slug";s:6:"upload";s:5:"count";s:3:"428";}s:12:"notification";a:3:{s:4:"name";s:12:"notification";s:4:"slug";s:12:"notification";s:5:"count";s:3:"428";}s:4:"news";a:3:{s:4:"name";s:4:"News";s:4:"slug";s:4:"news";s:5:"count";s:3:"426";}s:7:"sharing";a:3:{s:4:"name";s:7:"sharing";s:4:"slug";s:7:"sharing";s:5:"count";s:3:"424";}s:16:"custom-post-type";a:3:{s:4:"name";s:16:"custom post type";s:4:"slug";s:16:"custom-post-type";s:5:"count";s:3:"423";}s:5:"popup";a:3:{s:4:"name";s:5:"popup";s:4:"slug";s:5:"popup";s:5:"count";s:3:"421";}s:5:"flash";a:3:{s:4:"name";s:5:"flash";s:4:"slug";s:5:"flash";s:5:"count";s:3:"420";}}', 'no'),
(480, 'pb_backupbuddy', 'a:102:{s:12:"data_version";s:2:"14";s:21:"importbuddy_pass_hash";s:32:"00c942ce24363465d1512738de14125a";s:23:"importbuddy_pass_length";i:11;s:16:"backup_reminders";i:1;s:16:"edits_since_last";i:35;s:17:"last_backup_start";d:1486460896.4222929477691650390625;s:18:"last_backup_finish";d:1486460914.8700649738311767578125;s:18:"last_backup_serial";s:10:"s32hiai736";s:17:"last_backup_stats";a:10:{s:11:"archiveFile";s:62:"backup-workerassist_dev-2017_02_07-09_48am-full-s32hiai736.zip";s:10:"archiveURL";s:125:"http://workerassist.dev/wp-content/uploads/backupbuddy_backups/backup-workerassist_dev-2017_02_07-09_48am-full-s32hiai736.zip";s:11:"archiveSize";i:46436986;s:5:"start";d:1486460896.4222929477691650390625;s:6:"finish";d:1486460914.8700649738311767578125;s:4:"type";s:4:"full";s:12:"profileTitle";s:15:"Complete Backup";s:13:"scheduleTitle";s:0:"";s:15:"integrityStatus";b:1;s:12:"destinations";a:0:{}}s:21:"last_error_email_time";i:0;s:19:"force_compatibility";i:0;s:29:"force_mysqldump_compatibility";i:0;s:9:"schedules";a:0:{}s:9:"log_level";s:1:"1";s:13:"high_security";i:0;s:19:"next_schedule_index";i:100;s:13:"archive_limit";i:0;s:18:"archive_limit_full";i:0;s:16:"archive_limit_db";i:0;s:19:"archive_limit_files";i:0;s:18:"archive_limit_size";i:0;s:22:"archive_limit_size_big";i:50000;s:17:"archive_limit_age";i:0;s:26:"delete_archives_pre_backup";i:0;s:23:"lock_archives_directory";i:0;s:25:"set_greedy_execution_time";i:0;s:28:"email_notify_scheduled_start";s:0:"";s:36:"email_notify_scheduled_start_subject";s:49:"BackupBuddy Scheduled Backup Started - {home_url}";s:33:"email_notify_scheduled_start_body";s:140:"A scheduled backup has started with BackupBuddy v{backupbuddy_version} on {current_datetime} for the site {home_url}.\n\nDetails:\r\n\r\n{message}";s:31:"email_notify_scheduled_complete";s:0:"";s:39:"email_notify_scheduled_complete_subject";s:50:"BackupBuddy Scheduled Backup Complete - {home_url}";s:36:"email_notify_scheduled_complete_body";s:142:"A scheduled backup has completed with BackupBuddy v{backupbuddy_version} on {current_datetime} for the site {home_url}.\n\nDetails:\r\n\r\n{message}";s:24:"email_notify_send_finish";s:0:"";s:32:"email_notify_send_finish_subject";s:43:"BackupBuddy File Send Finished - {home_url}";s:29:"email_notify_send_finish_body";s:168:"A destination file send of file {backup_file} has finished with BackupBuddy v{backupbuddy_version} on {current_datetime} for the site {home_url}.\n\nDetails:\r\n\r\n{message}";s:18:"email_notify_error";s:22:"karloestrada@yahoo.com";s:26:"email_notify_error_subject";s:30:"BackupBuddy Error - {home_url}";s:23:"email_notify_error_body";s:132:"An error occurred with BackupBuddy v{backupbuddy_version} on {current_datetime} for the site {home_url}. Error details:\r\n\r\n{message}";s:12:"email_return";s:0:"";s:19:"remote_destinations";a:0:{}s:27:"remote_send_timeout_retries";s:1:"1";s:11:"role_access";s:16:"activate_plugins";s:16:"dropboxtemptoken";s:0:"";s:11:"backup_mode";s:1:"2";s:16:"multisite_export";s:1:"0";s:16:"backup_directory";s:0:"";s:14:"temp_directory";s:0:"";s:13:"log_directory";s:0:"";s:10:"log_serial";s:15:"euno7q4zx7juxm9";s:13:"notifications";a:0:{}s:19:"zip_method_strategy";s:1:"1";s:24:"database_method_strategy";s:3:"php";s:17:"alternative_zip_2";s:1:"0";s:19:"ignore_zip_warnings";s:1:"0";s:19:"ignore_zip_symlinks";s:1:"1";s:18:"zip_build_strategy";s:1:"3";s:15:"zip_step_period";s:2:"30";s:13:"zip_burst_gap";s:1:"2";s:21:"zip_min_burst_content";s:2:"10";s:21:"zip_max_burst_content";s:3:"100";s:25:"disable_zipmethod_caching";s:1:"0";s:19:"archive_name_format";s:8:"datetime";s:20:"archive_name_profile";s:1:"0";s:30:"disable_https_local_ssl_verify";s:1:"0";s:17:"save_comment_meta";s:1:"1";s:27:"ignore_command_length_check";s:1:"0";s:18:"default_backup_tab";s:1:"0";s:18:"deployment_allowed";s:1:"0";s:9:"hide_live";s:1:"0";s:10:"remote_api";a:2:{s:4:"keys";a:0:{}s:3:"ips";a:0:{}}s:20:"skip_spawn_cron_call";s:1:"0";s:5:"stats";a:6:{s:9:"site_size";i:0;s:18:"site_size_excluded";i:0;s:17:"site_size_updated";i:0;s:7:"db_size";i:0;s:16:"db_size_excluded";i:0;s:15:"db_size_updated";i:0;}s:9:"disalerts";a:1:{s:25:"backupbuddy_version_seven";i:1486515559;}s:15:"breakout_tables";s:1:"1";s:19:"include_importbuddy";s:1:"1";s:17:"max_site_log_size";s:1:"3";s:11:"compression";s:1:"1";s:25:"no_new_backups_error_days";s:2:"10";s:15:"skip_quicksetup";s:1:"0";s:13:"prevent_flush";s:1:"0";s:17:"rollback_cleanups";a:0:{}s:20:"phpmysqldump_maxrows";s:0:"";s:20:"disable_localization";s:1:"0";s:18:"max_execution_time";s:0:"";s:24:"backup_cron_rescheduling";s:1:"0";s:29:"backup_cron_passed_force_time";s:0:"";s:20:"force_single_db_file";s:1:"0";s:11:"deployments";a:0:{}s:19:"max_send_stats_days";s:1:"7";s:20:"max_send_stats_count";s:1:"6";s:26:"max_notifications_age_days";s:2:"21";s:19:"save_backup_sum_log";s:1:"1";s:26:"limit_single_cron_per_pass";s:1:"1";s:18:"tested_php_runtime";d:30;s:17:"tested_php_memory";d:127;s:23:"last_tested_php_runtime";i:1486455260;s:22:"last_tested_php_memory";i:1486455342;s:17:"use_internal_cron";s:1:"0";s:33:"php_runtime_test_minimum_interval";s:6:"604800";s:32:"php_memory_test_minimum_interval";s:6:"604800";s:29:"cron_request_timeout_override";s:0:"";s:8:"profiles";a:3:{i:0;a:9:{s:4:"type";s:8:"defaults";s:5:"title";s:15:"Global Defaults";s:18:"skip_database_dump";s:1:"0";s:19:"backup_nonwp_tables";s:1:"0";s:15:"integrity_check";s:1:"1";s:29:"mysqldump_additional_includes";s:0:"";s:29:"mysqldump_additional_excludes";s:0:"";s:8:"excludes";s:0:"";s:11:"custom_root";s:0:"";}i:1;a:3:{s:4:"type";s:2:"db";s:5:"title";s:13:"Database Only";s:3:"tip";s:49:"Just your database. I like your minimalist style.";}i:2;a:2:{s:4:"type";s:4:"full";s:5:"title";s:15:"Complete Backup";}}}', 'no'),
(484, 'ithemes-updater-cache', 'a:9:{s:18:"timeout-multiplier";i:1;s:10:"expiration";i:0;s:9:"timestamp";i:0;s:8:"packages";a:1:{i:0;s:11:"backupbuddy";}s:14:"update_plugins";a:0:{}s:13:"update_themes";a:0:{}s:12:"use_ca_patch";b:0;s:7:"use_ssl";b:1;s:14:"quick_releases";b:0;}', 'no'),
(485, '_transient_timeout_pb_backupbuddy_latest_version', '1486498435', 'no'),
(486, '_transient_pb_backupbuddy_latest_version', 'a:2:{i:0;s:7:"7.2.2.1";i:1;s:5:"7.2.2";}', 'no'),
(487, '_transient_timeout_pb_backupbuddy_avail_zip_methods', '1486498436', 'no'),
(488, '_transient_pb_backupbuddy_avail_zip_methods', 'a:3:{s:7:"methods";a:3:{i:0;s:4:"exec";i:1;s:10:"ziparchive";i:2;s:6:"pclzip";}s:7:"details";a:3:{s:4:"exec";a:2:{s:4:"attr";a:10:{s:4:"name";s:11:"Exec Method";s:13:"compatibility";b:0;s:10:"is_checker";b:1;s:9:"is_lister";b:1;s:11:"is_archiver";b:1;s:13:"is_unarchiver";b:1;s:12:"is_commenter";b:1;s:9:"is_zipper";b:1;s:11:"is_unzipper";b:1;s:12:"is_extractor";b:1;}s:5:"param";a:3:{s:3:"zip";a:4:{s:7:"version";a:2:{s:5:"major";i:3;s:5:"minor";i:0;}s:7:"options";s:0:"";s:4:"info";s:0:"";s:4:"path";s:0:"";}s:5:"unzip";a:4:{s:7:"version";a:2:{s:5:"major";i:6;s:5:"minor";i:0;}s:7:"options";s:0:"";s:4:"info";s:1138:"UnZip 6.00 of 20 April 2009, by Info-ZIP.  Maintained by C. Spieler.  Send\nbug reports using http://www.info-zip.org/zip-bug.html; see README for details.\n\nLatest sources and executables are at ftp://ftp.info-zip.org/pub/infozip/ ;\nsee ftp://ftp.info-zip.org/pub/infozip/UnZip.html for other sites.\n\nCompiled with gcc 4.2.1 Compatible Apple LLVM 8.0.0 (clang-800.0.34) for Unix Mac OS X on Jul 30 2016.\n\nUnZip special compilation options:\n        COPYRIGHT_CLEAN (PKZIP 0.9x unreducing method not supported)\n        SET_DIR_ATTRIB\n        SYMLINKS (symbolic links supported, if RTL and file system permit)\n        TIMESTAMP\n        UNIXBACKUP\n        USE_EF_UT_TIME\n        USE_UNSHRINK (PKZIP/Zip 1.x unshrinking method supported)\n        USE_DEFLATE64 (PKZIP 4.x Deflate64(tm) supported)\n        LARGE_FILE_SUPPORT (large files over 2 GiB supported)\n        ZIP64_SUPPORT (archives using Zip64 for large files supported)\n        VMS_TEXT_CONV\n        [decryption, version 2.11 of 05 Jan 2007]\n\nUnZip and ZipInfo environment options:\n           UNZIP:  [none]\n        UNZIPOPT:  [none]\n         ZIPINFO:  [none]\n      ZIPINFOOPT:  [none]";s:4:"path";s:0:"";}s:4:"path";s:0:"";}}s:10:"ziparchive";a:2:{s:4:"attr";a:10:{s:4:"name";s:17:"ZipArchive Method";s:13:"compatibility";b:0;s:10:"is_checker";b:1;s:9:"is_lister";b:1;s:11:"is_archiver";b:0;s:13:"is_unarchiver";b:1;s:12:"is_commenter";b:1;s:9:"is_zipper";b:1;s:11:"is_unzipper";b:1;s:12:"is_extractor";b:1;}s:5:"param";a:0:{}}s:6:"pclzip";a:2:{s:4:"attr";a:10:{s:4:"name";s:13:"PclZip Method";s:13:"compatibility";b:1;s:10:"is_checker";b:1;s:9:"is_lister";b:1;s:11:"is_archiver";b:1;s:13:"is_unarchiver";b:1;s:12:"is_commenter";b:1;s:9:"is_zipper";b:1;s:11:"is_unzipper";b:1;s:12:"is_extractor";b:1;}s:5:"param";a:0:{}}}s:7:"control";a:1:{s:9:"signature";s:32:"7fcd452e129d4196f117cbefc0c703ac";}}', 'no'),
(490, 'pb_backupbuddy_notifications', 'a:2:{i:0;a:7:{s:4:"time";i:1486455358;s:4:"slug";s:14:"backup_success";s:5:"title";s:30:"Backup completed successfully.";s:7:"message";s:56:"A manual backup has completed successfully on your site.";s:4:"data";a:3:{s:6:"serial";s:10:"8jbwgn5hjq";s:4:"type";s:4:"full";s:13:"profile_title";s:15:"Complete Backup";}s:6:"urgent";b:0;s:8:"syncSent";b:0;}i:1;a:7:{s:4:"time";i:1486460914;s:4:"slug";s:14:"backup_success";s:5:"title";s:30:"Backup completed successfully.";s:7:"message";s:56:"A manual backup has completed successfully on your site.";s:4:"data";a:3:{s:6:"serial";s:10:"s32hiai736";s:4:"type";s:4:"full";s:13:"profile_title";s:15:"Complete Backup";}s:6:"urgent";b:0;s:8:"syncSent";b:0;}}', 'no'),
(491, '_transient_timeout_pb_backupbuddy_stop_backup-8jbwgn5hjq', '1486541760', 'no'),
(492, '_transient_pb_backupbuddy_stop_backup-8jbwgn5hjq', '1', 'no'),
(500, '_transient_timeout_pb_backupbuddy_stop_backup-s32hiai736', '1486547315', 'no'),
(501, '_transient_pb_backupbuddy_stop_backup-s32hiai736', '1', 'no'),
(566, '_transient_is_multi_author', '0', 'yes'),
(577, '_transient_timeout_gform_update_info', '1486625674', 'no'),
(578, '_transient_gform_update_info', 'a:6:{s:12:"is_valid_key";s:1:"1";s:7:"version";s:5:"2.1.2";s:3:"url";s:164:"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=dSPbEQwScIGhBOzBvnrOVzbeWOE%3D";s:15:"expiration_time";i:1489640400;s:9:"offerings";a:42:{s:12:"gravityforms";a:3:{s:12:"is_available";b:1;s:7:"version";s:5:"2.1.2";s:3:"url";s:164:"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=dSPbEQwScIGhBOzBvnrOVzbeWOE%3D";}s:26:"gravityformsactivecampaign";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.4";s:3:"url";s:193:"http://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=omx06YVsNN3GrIY2SQ%2BWLx%2FPKek%3D";}s:20:"gravityformsagilecrm";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:177:"http://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=2miL7qICMLMF36VjnHzK9Tj4qnc%3D";}s:24:"gravityformsauthorizenet";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.3";s:3:"url";s:187:"http://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=FmDka1ywMDz6j7KfR%2Fm718MmPIY%3D";}s:18:"gravityformsaweber";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.4";s:3:"url";s:173:"http://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=YJnLlTs8D9LmoXzBCoG9dJ0h2FA%3D";}s:21:"gravityformsbatchbook";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.2";s:3:"url";s:183:"http://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=VFhB1VtjH%2B%2FfQ2eiPUX2wKk31e0%3D";}s:18:"gravityformsbreeze";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.2";s:3:"url";s:173:"http://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=glBcvonE4IiDMaFBX79nw1EErcc%3D";}s:27:"gravityformscampaignmonitor";a:3:{s:12:"is_available";b:1;s:7:"version";s:5:"3.4.1";s:3:"url";s:193:"http://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.4.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=aa54eMMBTrrReoHFsrAkvV247Ic%3D";}s:20:"gravityformscampfire";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:177:"http://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=cDmNTJBKx70hTSmFadUAkLlSBh0%3D";}s:22:"gravityformscapsulecrm";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:187:"http://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=p%2F4%2Bx%2BWGpMPySoxcwromeTQOy3Y%3D";}s:23:"gravityformscleverreach";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.3";s:3:"url";s:183:"http://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=fa0znx0ObakD3LgVcmEK6MQLSFc%3D";}s:19:"gravityformscoupons";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.4";s:3:"url";s:175:"http://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=AVnFDoD0eErd9tw9ufB1m0VaT8Q%3D";}s:17:"gravityformsdebug";a:3:{s:12:"is_available";b:1;s:7:"version";s:0:"";s:3:"url";s:0:"";}s:19:"gravityformsdropbox";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.3";s:3:"url";s:179:"http://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_1.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=QlqY68ol1Nj7h6BiZKp9%2F%2Bc4rcU%3D";}s:16:"gravityformsemma";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:169:"http://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=qLQ1mJS8cQLvEsl9FqZxNQtu8aU%3D";}s:22:"gravityformsfreshbooks";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.3";s:3:"url";s:185:"http://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=KaC2Gg9G23jkPPIV%2FErXmC7%2Fmo0%3D";}s:23:"gravityformsgetresponse";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:185:"http://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=PPo00VzbOCw%2F92sb9sxpAugPkR4%3D";}s:21:"gravityformshelpscout";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.4";s:3:"url";s:181:"http://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=92bD0wVY1Hxg9S2Ne%2BGgL62mQNY%3D";}s:20:"gravityformshighrise";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:181:"http://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=v5SdrduvmDCStvhV8Xgivb%2FeH9I%3D";}s:19:"gravityformshipchat";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:177:"http://s3.amazonaws.com/gravityforms/addons/hipchat/gravityformshipchat_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=X54X%2FXxObLW8dSO0MaDuhPajVzI%3D";}s:20:"gravityformsicontact";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.2";s:3:"url";s:177:"http://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=R9thCqcV7JkHawhIHIk75DM28vc%3D";}s:19:"gravityformslogging";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.0";s:3:"url";s:179:"http://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.0.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=Z33JqdsilQcvyp%2FE%2FNQmlcA8tMg%3D";}s:19:"gravityformsmadmimi";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.1";s:3:"url";s:179:"http://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=qkJipXT%2BZERjQcKed3Gq%2BvMTAPU%3D";}s:21:"gravityformsmailchimp";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"4.1";s:3:"url";s:179:"http://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=xmpwKImlYCaAAO7GTOKnJJBKxEg%3D";}s:26:"gravityformspartialentries";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.0";s:3:"url";s:191:"http://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.0.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=VyVJukZA3O2XethgVruF0Lt%2FcEs%3D";}s:18:"gravityformspaypal";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.8";s:3:"url";s:173:"http://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_2.8.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=JlCbwVc6LLCjKynocx4R4uYRjUc%3D";}s:33:"gravityformspaypalexpresscheckout";a:2:{s:12:"is_available";b:0;s:7:"version";s:0:"";}s:29:"gravityformspaypalpaymentspro";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.1";s:3:"url";s:197:"http://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=tiIV%2BZqU6KMvrDImrD7n9XcInII%3D";}s:21:"gravityformspaypalpro";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.7";s:3:"url";s:181:"http://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.7.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=2rsYWW3g%2FsXxtOInSmlmxETl5dU%3D";}s:20:"gravityformspicatcha";a:2:{s:12:"is_available";b:0;s:7:"version";s:3:"2.0";}s:17:"gravityformspolls";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"3.1";s:3:"url";s:173:"http://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=Rg%2Bnx5hq9FliKIWy8I2MsnfPJOA%3D";}s:16:"gravityformsquiz";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"3.1";s:3:"url";s:169:"http://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=1PtdHR7VSn0B7VcvaCkK4DsQ3ME%3D";}s:19:"gravityformsrestapi";a:3:{s:12:"is_available";b:1;s:7:"version";s:10:"2.0-beta-2";s:3:"url";s:182:"http://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=ep7YpQ4yBHnmLDLolxKWD2zrcdk%3D";}s:21:"gravityformssignature";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"3.3";s:3:"url";s:179:"http://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=DzOysyCmM9x7eMdE39iCBv0SJOA%3D";}s:17:"gravityformsslack";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.6";s:3:"url";s:175:"http://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.6.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=AhgDmEsIXp%2FIFKHoJy6GpAC%2Bygo%3D";}s:18:"gravityformsstripe";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.2";s:3:"url";s:173:"http://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_2.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=fgmsmMd5ZXvs4SGiU5CUvJliI1I%3D";}s:18:"gravityformssurvey";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"3.2";s:3:"url";s:173:"http://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=6llF3VXosD4ppSYiOq5Jaqf40FQ%3D";}s:18:"gravityformstrello";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.2";s:3:"url";s:175:"http://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=Ixt1ahBqZR%2B3rvOHRIIw70f4Dtw%3D";}s:18:"gravityformstwilio";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.2";s:3:"url";s:175:"http://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=eoNn%2B4rUMsNRIkP01dRvVUYkIIM%3D";}s:28:"gravityformsuserregistration";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"3.6";s:3:"url";s:197:"http://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_3.6.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=D2%2BZAg6zWs0lqWv1svpAaCtq%2BA8%3D";}s:18:"gravityformszapier";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"2.1";s:3:"url";s:175:"http://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=58K0TSqIK1FCi%2BQ3KzI13rvoPxc%3D";}s:19:"gravityformszohocrm";a:3:{s:12:"is_available";b:1;s:7:"version";s:3:"1.4";s:3:"url";s:175:"http://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1486712047&Signature=I9VxZAJ90LWtzfqsFgmvZmr5B1o%3D";}}s:9:"is_active";s:1:"1";}', 'no'),
(579, '_transient_timeout_acf_get_remote_plugin_info', '1486625676', 'no'),
(580, '_transient_acf_get_remote_plugin_info', 'a:12:{s:4:"name";s:26:"Advanced Custom Fields PRO";s:4:"slug";s:26:"advanced-custom-fields-pro";s:8:"homepage";s:37:"https://www.advancedcustomfields.com/";s:7:"version";s:5:"5.5.7";s:6:"author";s:13:"Elliot Condon";s:10:"author_url";s:28:"http://www.elliotcondon.com/";s:12:"contributors";s:12:"elliotcondon";s:8:"requires";s:5:"3.6.0";s:6:"tested";s:5:"4.7.0";s:6:"tagged";s:123:"acf, advanced, custom, field, fields, custom field, custom fields, simple fields, magic fields, more fields, repeater, edit";s:9:"changelog";s:134:"<h4>5.5.7</h4><ul><li>Core: Fixed bug causing `get_field()` to return incorrect data for sub fields registered via PHP code.</li></ul>";s:14:"upgrade_notice";s:0:"";}', 'no'),
(581, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1486539280;s:8:"response";a:1:{s:34:"advanced-custom-fields-pro/acf.php";O:8:"stdClass":5:{s:4:"slug";s:26:"advanced-custom-fields-pro";s:6:"plugin";s:34:"advanced-custom-fields-pro/acf.php";s:11:"new_version";s:5:"5.5.7";s:3:"url";s:37:"https://www.advancedcustomfields.com/";s:7:"package";s:0:"";}}s:12:"translations";a:0:{}s:9:"no_update";a:3:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:3:"3.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:54:"https://downloads.wordpress.org/plugin/akismet.3.2.zip";}s:78:"gravity-forms-multiple-form-instances/gravityforms-multiple-form-instances.php";O:8:"stdClass":6:{s:2:"id";s:5:"56112";s:4:"slug";s:37:"gravity-forms-multiple-form-instances";s:6:"plugin";s:78:"gravity-forms-multiple-form-instances/gravityforms-multiple-form-instances.php";s:11:"new_version";s:5:"1.1.1";s:3:"url";s:68:"https://wordpress.org/plugins/gravity-forms-multiple-form-instances/";s:7:"package";s:86:"https://downloads.wordpress.org/plugin/gravity-forms-multiple-form-instances.1.1.1.zip";}s:9:"hello.php";O:8:"stdClass":6:{s:2:"id";s:4:"3564";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";}}}', 'no'),
(588, '_transient_timeout_feed_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1486595273', 'no'),
(589, '_transient_timeout_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1486595273', 'no'),
(590, '_transient_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1486552073', 'no'),
(591, '_transient_timeout_feed_d117b5738fbd35bd8c0391cda1f2b5d9', '1486595274', 'no'),
(592, '_transient_timeout_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1486595274', 'no'),
(593, '_transient_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1486552074', 'no'),
(594, '_transient_timeout_feed_b9388c83948825c1edaef0d856b7b109', '1486595276', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(595, '_transient_feed_b9388c83948825c1edaef0d856b7b109', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n	\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:117:"\n		\n		\n		\n		\n		\n		\n				\n\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"WordPress Plugins » View: Popular";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:45:"https://wordpress.org/plugins/browse/popular/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:34:"WordPress Plugins » View: Popular";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 08 Feb 2017 11:02:50 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:25:"http://bbpress.org/?v=1.1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:30:{i:0;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"WooCommerce";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/woocommerce/#post-29860";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Sep 2011 08:13:36 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"29860@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:97:"WooCommerce is a powerful, extendable eCommerce plugin that helps you sell anything. Beautifully.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"WooThemes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:24:"Jetpack by WordPress.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"https://wordpress.org/plugins/jetpack/#post-23862";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 Jan 2011 02:21:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"23862@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:148:"The one plugin you need for stats, related posts, search engine optimization, social sharing, protection, backups, speed, and email list management.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Automattic";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:9:"Yoast SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://wordpress.org/plugins/wordpress-seo/#post-8321";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 01 Jan 2009 20:34:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"8321@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:118:"Improve your WordPress SEO: Write better content and have a fully optimized WordPress site using the Yoast SEO plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Joost de Valk";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Really Simple CAPTCHA";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"https://wordpress.org/plugins/really-simple-captcha/#post-9542";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 09 Mar 2009 02:17:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"9542@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:138:"Really Simple CAPTCHA is a CAPTCHA module intended to be called from other plugins. It is originally created for my Contact Form 7 plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Takayuki Miyoshi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"W3 Total Cache";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/plugins/w3-total-cache/#post-12073";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 29 Jul 2009 18:46:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"12073@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:144:"Search Engine (SEO) &#38; Performance Optimization (WPO) via caching. Integrated caching: CDN, Minify, Page, Object, Fragment, Database support.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Frederick Townes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"TinyMCE Advanced";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://wordpress.org/plugins/tinymce-advanced/#post-2082";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 27 Jun 2007 15:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"2082@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:58:"Extends and enhances TinyMCE, the WordPress Visual Editor.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"All in One SEO Pack";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/all-in-one-seo-pack/#post-753";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 30 Mar 2007 20:08:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"753@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:83:"The original SEO plugin for WordPress, downloaded over 30,000,000 times since 2007.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:8:"uberdose";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Duplicate Post";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/duplicate-post/#post-2646";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 05 Dec 2007 17:40:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"2646@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:22:"Clone posts and pages.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Lopo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Contact Form 7";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/contact-form-7/#post-2141";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 02 Aug 2007 12:45:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"2141@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:54:"Just another contact form plugin. Simple but flexible.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Takayuki Miyoshi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"Wordfence Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/wordfence/#post-29832";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 04 Sep 2011 03:13:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"29832@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:149:"Secure your website with the most comprehensive WordPress security plugin. Firewall, malware scan, blocking, live traffic, login security &#38; more.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Wordfence";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"Google Analytics by MonsterInsights";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/plugins/google-analytics-for-wordpress/#post-2316";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 14 Sep 2007 12:15:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"2316@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:113:"Connect Google Analytics with WordPress by adding your Google Analytics tracking code. Get the stats that matter.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Syed Balkhi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Regenerate Thumbnails";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"https://wordpress.org/plugins/regenerate-thumbnails/#post-6743";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 23 Aug 2008 14:38:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"6743@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:76:"Allows you to regenerate your thumbnails after changing the thumbnail sizes.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:25:"Alex Mills (Viper007Bond)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"WP Super Cache";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/wp-super-cache/#post-2572";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Nov 2007 11:40:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"2572@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:73:"A very fast caching engine for WordPress that produces static html files.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Donncha O Caoimh";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Hello Dolly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://wordpress.org/plugins/hello-dolly/#post-5790";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 29 May 2008 22:11:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"5790@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:150:"This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Matt Mullenweg";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"NextGEN Gallery - WordPress Gallery Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/plugins/nextgen-gallery/#post-1169";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Apr 2007 20:08:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"1169@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:121:"The most popular WordPress gallery plugin and one of the most popular plugins of all time with over 16 million downloads.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Alex Rabe";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"UpdraftPlus WordPress Backup Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/updraftplus/#post-38058";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 21 May 2012 15:14:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"38058@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:148:"Backup and restoration made easy. Complete backups; manual or scheduled (backup to S3, Dropbox, Google Drive, Rackspace, FTP, SFTP, email + others).";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"David Anderson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Clef Two-Factor Authentication";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://wordpress.org/plugins/wpclef/#post-47509";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 27 Dec 2012 01:25:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"47509@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:139:"Modern two-factor that people love to use: strong authentication without passwords or tokens; single sign on/off; magical login experience.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Dave Ross";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"WordPress Importer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/wordpress-importer/#post-18101";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 May 2010 17:42:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"18101@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:101:"Import posts, pages, comments, custom fields, categories, tags and more from a WordPress export file.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Brian Colinger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:7:"Akismet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:46:"https://wordpress.org/plugins/akismet/#post-15";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 22:11:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"15@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:98:"Akismet checks your comments against the Akismet Web service to see if they look like spam or not.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Matt Mullenweg";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:26:"Page Builder by SiteOrigin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/siteorigin-panels/#post-51888";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 11 Apr 2013 10:36:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"51888@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:111:"Build responsive page layouts using the widgets you know and love using this simple drag and drop page builder.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Greg Priday";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"Advanced Custom Fields";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wordpress.org/plugins/advanced-custom-fields/#post-25254";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Mar 2011 04:07:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"25254@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:68:"Customise WordPress with powerful, professional and intuitive fields";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"elliotcondon";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"WP-PageNavi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/wp-pagenavi/#post-363";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 23:17:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"363@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:49:"Adds a more advanced paging navigation interface.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Lester Chan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"Google XML Sitemaps";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wordpress.org/plugins/google-sitemap-generator/#post-132";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 22:31:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"132@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:105:"This plugin will generate a special XML sitemap which will help search engines to better index your blog.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Arne Brachhold";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"Google Analytics Dashboard for WP";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"https://wordpress.org/plugins/google-analytics-dashboard-for-wp/#post-50539";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 10 Mar 2013 17:07:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"50539@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:125:"Displays Google Analytics stats in your WordPress Dashboard. Inserts the latest Google Analytics tracking code in your pages.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Alin Marcu";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"Disable Comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wordpress.org/plugins/disable-comments/#post-26907";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 May 2011 04:42:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"26907@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:150:"Allows administrators to globally disable comments on their site. Comments can be disabled according to post type. Multisite friendly. Provides tool t";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Samir Shah";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:10:"Duplicator";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://wordpress.org/plugins/duplicator/#post-26607";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 May 2011 12:15:41 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"26607@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:88:"Duplicate, clone, backup, move and transfer an entire site from one location to another.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Cory Lamle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"WP Multibyte Patch";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/wp-multibyte-patch/#post-28395";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 14 Jul 2011 12:22:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"28395@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:71:"Multibyte functionality enhancement for the WordPress Japanese package.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"plugin-master";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"Black Studio TinyMCE Widget";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"https://wordpress.org/plugins/black-studio-tinymce-widget/#post-31973";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 10 Nov 2011 15:06:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"31973@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:39:"The visual editor widget for Wordpress.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Marco Chiesi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"SiteOrigin Widgets Bundle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/so-widgets-bundle/#post-67824";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 24 May 2014 14:27:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"67824@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:128:"A collection of all widgets, neatly bundled into a single plugin. It&#039;s also a framework to code your own widgets on top of.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Greg Priday";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"iThemes Security (formerly Better WP Security)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/better-wp-security/#post-21738";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 22 Oct 2010 22:06:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"21738@http://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:146:"Take the guesswork out of WordPress security. iThemes Security offers 30+ ways to lock down WordPress in an easy-to-use WordPress security plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"iThemes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:46:"https://wordpress.org/plugins/rss/view/popular";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:9:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Wed, 08 Feb 2017 11:07:56 GMT";s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"vary";s:15:"Accept-Encoding";s:25:"strict-transport-security";s:11:"max-age=360";s:13:"last-modified";s:29:"Mon, 05 Sep 2011 08:13:36 GMT";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:11:"HIT lax 249";s:16:"content-encoding";s:4:"gzip";}}s:5:"build";s:14:"20170131113045";}', 'no'),
(596, '_transient_timeout_feed_mod_b9388c83948825c1edaef0d856b7b109', '1486595276', 'no'),
(597, '_transient_feed_mod_b9388c83948825c1edaef0d856b7b109', '1486552076', 'no'),
(598, '_transient_timeout_plugin_slugs', '1486638476', 'no'),
(599, '_transient_plugin_slugs', 'a:6:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:19:"akismet/akismet.php";i:2;s:27:"backupbuddy/backupbuddy.php";i:3;s:29:"gravityforms/gravityforms.php";i:4;s:78:"gravity-forms-multiple-form-instances/gravityforms-multiple-form-instances.php";i:5;s:9:"hello.php";}', 'no'),
(600, '_transient_timeout_dash_88ae138922fe95674369b1cb3d215a2b', '1486595276', 'no'),
(601, '_transient_dash_88ae138922fe95674369b1cb3d215a2b', '<div class="rss-widget"><ul><li><a class=\'rsswidget\' href=\'https://wordpress.org/news/2017/01/wordpress-4-7-2-security-release/\'>WordPress 4.7.2 Security Release</a> <span class="rss-date">January 26, 2017</span><div class="rssSummary">WordPress 4.7.2 is now available. This is a security release for all previous versions and we strongly encourage you to update your sites immediately. WordPress versions 4.7.1 and earlier are affected by three security issues: The user interface for assigning taxonomy terms in Press This is shown to users who do not have permissions to use it. [&hellip;]</div></li></ul></div><div class="rss-widget"><ul><li><a class=\'rsswidget\' href=\'https://wptavern.com/wp-super-cache-1-4-9-patches-multiple-xss-vulnerabilities\'>WPTavern: WP Super Cache 1.4.9 Patches Multiple XSS Vulnerabilities</a></li><li><a class=\'rsswidget\' href=\'https://wptavern.com/wordpress-for-android-closes-google-community-for-beta-testers\'>WPTavern: WordPress for Android Closes Google+ Community for Beta Testers</a></li><li><a class=\'rsswidget\' href=\'https://wptavern.com/google-updates-amp-to-allow-sharing-of-canonical-url\'>WPTavern: Google Updates AMP to Allow Sharing of Canonical URL</a></li></ul></div><div class="rss-widget"><ul><li class="dashboard-news-plugin"><span>Popular Plugin:</span> SiteOrigin Widgets Bundle&nbsp;<a href="plugin-install.php?tab=plugin-information&amp;plugin=so-widgets-bundle&amp;_wpnonce=2e702d6fc7&amp;TB_iframe=true&amp;width=600&amp;height=800" class="thickbox open-plugin-details-modal" aria-label="Install SiteOrigin Widgets Bundle">(Install)</a></li></ul></div>', 'no'),
(602, '_transient_timeout_GFCache_bb3cd1b3285c2d0f012af3502c78115f', '1486552124', 'no'),
(603, '_transient_GFCache_bb3cd1b3285c2d0f012af3502c78115f', 'a:0:{}', 'no'),
(604, '_transient_timeout_GFCache_d4c2968c300d4d659dd93ccee52f0064', '1486552124', 'no'),
(605, '_transient_GFCache_d4c2968c300d4d659dd93ccee52f0064', 'a:1:{i:0;O:8:"stdClass":2:{s:7:"form_id";s:1:"1";s:10:"view_count";s:3:"546";}}', 'no'),
(606, 'options_homepage_show_next_step_graphic', '1', 'no'),
(607, '_options_homepage_show_next_step_graphic', 'field_58983d871b07c', 'no'),
(608, 'options_homepage_next_step_image', '31', 'no'),
(609, '_options_homepage_next_step_image', 'field_589a91bda3c4b', 'no'),
(610, 'options_resource_show_next_step_graphic', '0', 'no'),
(611, '_options_resource_show_next_step_graphic', 'field_589a9185a3c4a', 'no'),
(612, 'options_resource_next_step_image', '', 'no'),
(613, '_options_resource_next_step_image', 'field_589846e602cd7', 'no'),
(614, 'options_news_show_next_step_graphic', '0', 'no'),
(615, '_options_news_show_next_step_graphic', 'field_589a9218b42d8', 'no'),
(616, 'options_news_next_step_image', '', 'no'),
(617, '_options_news_next_step_image', 'field_589a922fb42d9', 'no'),
(618, '_site_transient_timeout_theme_roots', '1486555711', 'no'),
(619, '_site_transient_theme_roots', 'a:1:{s:12:"workerassist";s:7:"/themes";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1486337548:1'),
(6, 7, '_edit_last', '1'),
(7, 7, '_edit_lock', '1486361154:1'),
(8, 7, '_wp_page_template', 'news.php'),
(9, 9, '_edit_last', '1'),
(10, 9, '_edit_lock', '1486383319:1'),
(11, 15, '_edit_last', '1'),
(12, 15, 'casestudies_image', ''),
(13, 15, '_casestudies_image', 'field_58981385feaab'),
(14, 15, 'casestudies_name', ''),
(15, 15, '_casestudies_name', 'field_589813a2feaac'),
(16, 15, 'casestudies_textintro', ''),
(17, 15, '_casestudies_textintro', 'field_58981b76b88a3'),
(18, 15, 'casestudies_solutions', ''),
(19, 15, '_casestudies_solutions', 'field_58981b9ab94d0'),
(20, 15, '_edit_lock', '1486363527:1'),
(21, 16, '_edit_last', '1'),
(22, 16, '_edit_lock', '1486552073:1'),
(23, 31, '_wp_attached_file', '2017/02/next-step.png'),
(24, 31, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:371;s:6:"height";i:290;s:4:"file";s:21:"2017/02/next-step.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"next-step-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"next-step-300x235.png";s:5:"width";i:300;s:6:"height";i:235;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(25, 32, '_wp_attached_file', '2017/02/banner1.jpg'),
(26, 32, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:517;s:4:"file";s:19:"2017/02/banner1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"banner1-300x97.jpg";s:5:"width";i:300;s:6:"height";i:97;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner1-768x248.jpg";s:5:"width";i:768;s:6:"height";i:248;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner1-1024x331.jpg";s:5:"width";i:1024;s:6:"height";i:331;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(27, 33, '_edit_last', '1'),
(28, 33, '_edit_lock', '1486383987:1'),
(29, 40, '_edit_last', '1'),
(30, 40, '_edit_lock', '1486381541:1'),
(31, 41, '_wp_attached_file', '2017/02/casestudy_lin.jpg'),
(32, 41, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:370;s:6:"height";i:334;s:4:"file";s:25:"2017/02/casestudy_lin.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"casestudy_lin-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:25:"casestudy_lin-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(33, 42, '_wp_attached_file', '2017/02/casestudy_martin.jpg'),
(34, 42, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:370;s:6:"height";i:334;s:4:"file";s:28:"2017/02/casestudy_martin.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"casestudy_martin-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"casestudy_martin-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(35, 43, '_wp_attached_file', '2017/02/casestudy_gemma.jpg'),
(36, 43, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:370;s:6:"height";i:334;s:4:"file";s:27:"2017/02/casestudy_gemma.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"casestudy_gemma-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"casestudy_gemma-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(37, 44, '_wp_attached_file', '2017/02/image-service3.jpg'),
(38, 44, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:370;s:6:"height";i:334;s:4:"file";s:26:"2017/02/image-service3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"image-service3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"image-service3-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(39, 45, '_wp_attached_file', '2017/02/image-service2.jpg'),
(40, 45, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:370;s:6:"height";i:334;s:4:"file";s:26:"2017/02/image-service2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"image-service2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"image-service2-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(41, 46, '_wp_attached_file', '2017/02/image-service1.jpg'),
(42, 46, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:370;s:6:"height";i:334;s:4:"file";s:26:"2017/02/image-service1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"image-service1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:26:"image-service1-300x271.jpg";s:5:"width";i:300;s:6:"height";i:271;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(43, 40, 'casestudies_image', '43'),
(44, 40, '_casestudies_image', 'field_58981385feaab'),
(45, 40, 'casestudies_name', 'Gemma'),
(46, 40, '_casestudies_name', 'field_589813a2feaac'),
(47, 40, 'casestudies_textintro', '<strong>When Gemma hurt herself lifting boxes she wasn’t sure what to do with her workers compensation certificate.</strong>\r\n\r\nAfter a night searching for answers online, Gemma asked us about the next steps in making her claim.'),
(48, 40, '_casestudies_textintro', 'field_58981b76b88a3'),
(49, 40, 'casestudies_solutions', 'Martin made a workers compensation claim after years of constant verbal abuse from his manager. His doctor supported his claim. However, his manager disputed his claim. Martin got a section 81A referral in the mail – a legal letter saying that the inurer doesn’t think they should pay compensation – and didn’t know what the next steps were.\r\n\r\nWe explained his options and let him know about the process with the Tribunal. After the dispute was made, Martin’s weekly payments and medical expense payments were stopped. We helped Martin make a referral to the Workers Rehabilitation and Compensation Tribunal and his weekly payments and medicals were reinstated.'),
(50, 40, '_casestudies_solutions', 'field_58981b9ab94d0'),
(51, 47, '_edit_last', '1'),
(52, 47, '_edit_lock', '1486383674:1'),
(53, 47, 'casestudies_image', '42'),
(54, 47, '_casestudies_image', 'field_58981385feaab'),
(55, 47, 'casestudies_name', 'Martin'),
(56, 47, '_casestudies_name', 'field_589813a2feaac'),
(57, 47, 'casestudies_textintro', '<strong>Martin made a workers compensation claim after years of constant verbal abuse from his manager. </strong>\r\n\r\nWhen his claim was disputed he didn’t know what the next steps were, and contacted Worker Assist for advice. '),
(58, 47, '_casestudies_textintro', 'field_58981b76b88a3'),
(59, 47, 'casestudies_solutions', 'Martin made a workers compensation claim after years of constant verbal abuse from his manager. His doctor supported his claim. However, his manager disputed his claim. Martin got a section 81A referral in the mail – a legal letter saying that the inurer doesn’t think they should pay compensation – and didn’t know what the next steps were. \r\n\r\nWe explained his options and let him know about the process with the Tribunal. After the dispute was made, Martin’s weekly payments and medical expense payments were stopped. We helped Martin make a referral to the Workers Rehabilitation and Compensation Tribunal and his weekly payments and medicals were reinstated.\r\n'),
(60, 47, '_casestudies_solutions', 'field_58981b9ab94d0'),
(61, 48, '_edit_last', '1'),
(62, 48, '_edit_lock', '1486382980:1'),
(63, 48, 'casestudies_image', '41'),
(64, 48, '_casestudies_image', 'field_58981385feaab'),
(65, 48, 'casestudies_name', 'Lin'),
(66, 48, '_casestudies_name', 'field_589813a2feaac'),
(67, 48, 'casestudies_textintro', '<strong>Lin had a gap in her medical certificates after hurting her hand and shoulder at work. </strong>\r\n\r\nShe was confused about the next steps, especially because English is her second language, and asked us for help. '),
(68, 48, '_casestudies_textintro', 'field_58981b76b88a3'),
(69, 48, 'casestudies_solutions', 'Martin made a workers compensation claim after years of constant verbal abuse from his manager. His doctor supported his claim. However, his manager disputed his claim. Martin got a section 81A referral in the mail – a legal letter saying that the inurer doesn’t think they should pay compensation – and didn’t know what the next steps were. \r\n\r\nWe explained his options and let him know about the process with the Tribunal. After the dispute was made, Martin’s weekly payments and medical expense payments were stopped. We helped Martin make a referral to the Workers Rehabilitation and Compensation Tribunal and his weekly payments and medicals were reinstated.\r\n'),
(70, 48, '_casestudies_solutions', 'field_58981b9ab94d0'),
(71, 50, '_edit_last', '1'),
(72, 50, '_edit_lock', '1486437398:1'),
(73, 53, '_edit_last', '1'),
(74, 53, '_edit_lock', '1486423005:1'),
(75, 53, 'print_count', ''),
(76, 53, '_print_count', 'field_5898f7b9e8077'),
(77, 54, '_edit_last', '1'),
(78, 54, '_edit_lock', '1486423154:1'),
(79, 54, 'print_count', ''),
(80, 54, '_print_count', 'field_5898f7b9e8077'),
(81, 55, '_edit_last', '1'),
(82, 55, '_edit_lock', '1486508181:1'),
(83, 55, 'print_count', '6'),
(84, 55, '_print_count', 'field_5898f7b9e8077'),
(85, 57, '_edit_last', '1'),
(86, 57, '_edit_lock', '1486508183:1'),
(87, 57, 'print_count', '48'),
(88, 57, '_print_count', 'field_5898f7b9e8077'),
(89, 58, '_edit_last', '1'),
(90, 58, '_edit_lock', '1486424984:1'),
(94, 60, '_edit_last', '1'),
(95, 60, '_edit_lock', '1486424733:1'),
(97, 1, '_wp_trash_meta_status', 'publish'),
(98, 1, '_wp_trash_meta_time', '1486424897'),
(99, 1, '_wp_desired_post_slug', 'hello-world'),
(100, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:"1";}'),
(102, 15, '_wp_trash_meta_status', 'publish'),
(103, 15, '_wp_trash_meta_time', '1486425131'),
(104, 15, '_wp_desired_post_slug', '15-2'),
(105, 65, '_edit_last', '1'),
(106, 65, '_edit_lock', '1486427633:1'),
(109, 70, '_wp_attached_file', '2017/02/banner3.jpg'),
(110, 70, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:517;s:4:"file";s:19:"2017/02/banner3.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"banner3-300x97.jpg";s:5:"width";i:300;s:6:"height";i:97;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner3-768x248.jpg";s:5:"width";i:768;s:6:"height";i:248;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner3-1024x331.jpg";s:5:"width";i:1024;s:6:"height";i:331;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(111, 71, '_wp_attached_file', '2017/02/banner4.jpg'),
(112, 71, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2122;s:6:"height";i:918;s:4:"file";s:19:"2017/02/banner4.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"banner4-300x130.jpg";s:5:"width";i:300;s:6:"height";i:130;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner4-768x332.jpg";s:5:"width";i:768;s:6:"height";i:332;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner4-1024x443.jpg";s:5:"width";i:1024;s:6:"height";i:443;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(115, 73, '_edit_last', '1'),
(116, 73, '_edit_lock', '1486518646:1'),
(117, 73, '_wp_page_template', 'print.php');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext,
  `post_title` text,
  `post_excerpt` text,
  `post_status` varchar(20) DEFAULT 'publish',
  `comment_status` varchar(20) DEFAULT 'open',
  `ping_status` varchar(20) DEFAULT 'open',
  `post_password` varchar(255) DEFAULT NULL,
  `post_name` varchar(200) DEFAULT NULL,
  `to_ping` text,
  `pinged` text,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) DEFAULT NULL,
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) DEFAULT 'post',
  `post_mime_type` varchar(100) DEFAULT NULL,
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-01-31 07:23:21', '2017-01-31 07:23:21', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2017-02-06 23:48:17', '2017-02-06 23:48:17', '', 0, 'http://workerassist.dev/?p=1', 0, 'post', '', 1),
(2, 1, '2017-01-31 07:23:21', '2017-01-31 07:23:21', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://workerassist.dev/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2017-01-31 07:23:21', '2017-01-31 07:23:21', '', 0, 'http://workerassist.dev/?page_id=2', 0, 'page', '', 0),
(4, 1, '2017-02-05 23:32:31', '2017-02-05 23:32:31', '', 'Resources', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2017-02-05 23:32:31', '2017-02-05 23:32:31', '', 0, 'http://workerassist.dev/?page_id=4', 0, 'page', '', 0),
(5, 1, '2017-02-05 23:30:43', '2017-02-05 23:30:43', '', 'Resources', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-02-05 23:30:43', '2017-02-05 23:30:43', '', 4, 'http://workerassist.dev/2017/02/05/4-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2017-02-06 05:29:03', '2017-02-06 05:29:03', '', 'News & Events', '', 'publish', 'closed', 'closed', '', 'news-events', '', '', '2017-02-06 05:29:03', '2017-02-06 05:29:03', '', 0, 'http://workerassist.dev/?page_id=7', 0, 'page', '', 0),
(8, 1, '2017-02-06 05:29:03', '2017-02-06 05:29:03', '', 'News & Events', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2017-02-06 05:29:03', '2017-02-06 05:29:03', '', 7, 'http://workerassist.dev/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2017-02-06 06:45:07', '2017-02-06 06:45:07', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:11:"casestudies";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";a:15:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:15:"page_attributes";i:11;s:14:"featured_image";i:12;s:10:"categories";i:13;s:4:"tags";i:14;s:15:"send-trackbacks";}s:11:"description";s:0:"";}', 'Case Studies', 'case-studies', 'publish', 'closed', 'closed', '', 'group_5898133c817e2', '', '', '2017-02-06 11:39:38', '2017-02-06 11:39:38', '', 0, 'http://workerassist.dev/?post_type=acf-field-group&#038;p=9', 0, 'acf-field-group', '', 0),
(10, 1, '2017-02-06 06:45:07', '2017-02-06 06:45:07', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Image', 'casestudies_image', 'publish', 'closed', 'closed', '', 'field_58981385feaab', '', '', '2017-02-06 06:45:07', '2017-02-06 06:45:07', '', 9, 'http://workerassist.dev/?post_type=acf-field&p=10', 0, 'acf-field', '', 0),
(11, 1, '2017-02-06 06:45:07', '2017-02-06 06:45:07', 'a:10:{s:13:"default_value";s:0:"";s:9:"maxlength";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Name', 'casestudies_name', 'publish', 'closed', 'closed', '', 'field_589813a2feaac', '', '', '2017-02-06 06:45:07', '2017-02-06 06:45:07', '', 9, 'http://workerassist.dev/?post_type=acf-field&p=11', 1, 'acf-field', '', 0),
(12, 1, '2017-02-06 06:45:42', '2017-02-06 06:45:42', 'a:10:{s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";s:5:"delay";i:0;s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Text Intro', 'casestudies_textintro', 'publish', 'closed', 'closed', '', 'field_58981b76b88a3', '', '', '2017-02-06 11:38:15', '2017-02-06 11:38:15', '', 9, 'http://workerassist.dev/?post_type=acf-field&#038;p=12', 2, 'acf-field', '', 0),
(13, 1, '2017-02-06 06:46:17', '2017-02-06 06:46:17', 'a:10:{s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";s:5:"delay";i:0;s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Solutions', 'casestudies_solutions', 'publish', 'closed', 'closed', '', 'field_58981b9ab94d0', '', '', '2017-02-06 11:39:38', '2017-02-06 11:39:38', '', 9, 'http://workerassist.dev/?post_type=acf-field&#038;p=13', 3, 'acf-field', '', 0),
(14, 1, '2017-02-06 06:46:35', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-02-06 06:46:35', '0000-00-00 00:00:00', '', 0, 'http://workerassist.dev/?post_type=casestudies&p=14', 0, 'casestudies', '', 0),
(15, 1, '2017-02-06 06:47:48', '2017-02-06 06:47:48', '', '', '', 'trash', 'closed', 'closed', '', '15-2__trashed', '', '', '2017-02-06 23:52:11', '2017-02-06 23:52:11', '', 0, 'http://workerassist.dev/?post_type=casestudies&#038;p=15', 0, 'casestudies', '', 0),
(16, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:22:"theme-general-settings";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Banners', 'banners', 'publish', 'closed', 'closed', '', 'group_58983d7cd9e38', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 0, 'http://workerassist.dev/?post_type=acf-field-group&#038;p=16', 0, 'acf-field-group', '', 0),
(17, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:10:{s:13:"default_value";i:0;s:7:"message";s:0:"";s:2:"ui";i:0;s:10:"ui_on_text";s:0:"";s:11:"ui_off_text";s:0:"";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Show Next Step Graphic', 'homepage_show_next_step_graphic', 'publish', 'closed', 'closed', '', 'field_58983d871b07c', '', '', '2017-02-08 03:35:48', '2017-02-08 03:35:48', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=17', 1, 'acf-field', '', 0),
(18, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:10:{s:13:"default_value";i:0;s:7:"message";s:0:"";s:2:"ui";i:0;s:10:"ui_on_text";s:0:"";s:11:"ui_off_text";s:0:"";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Enable Homepage Slider', 'enable_homepage_slider', 'publish', 'closed', 'closed', '', 'field_58983db11b07d', '', '', '2017-02-08 03:35:48', '2017-02-08 03:35:48', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=18', 3, 'acf-field', '', 0),
(19, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:10:{s:13:"default_value";i:0;s:7:"message";s:0:"";s:2:"ui";i:0;s:10:"ui_on_text";s:0:"";s:11:"ui_off_text";s:0:"";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Enable  Resource Slider', 'enable__resource_slider', 'publish', 'closed', 'closed', '', 'field_58983dc91b07e', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=19', 8, 'acf-field', '', 0),
(20, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:10:{s:13:"default_value";i:0;s:7:"message";s:0:"";s:2:"ui";i:0;s:10:"ui_on_text";s:0:"";s:11:"ui_off_text";s:0:"";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Enable News Slider', 'enable_news_slider', 'publish', 'closed', 'closed', '', 'field_58983de01b07f', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=20', 13, 'acf-field', '', 0),
(21, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:10:{s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";s:9:"collapsed";s:0:"";s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58983db11b07d";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}}}', 'Homepage Slider', 'homepage_slider', 'publish', 'closed', 'closed', '', 'field_58983df81b080', '', '', '2017-02-08 03:35:48', '2017-02-08 03:35:48', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=21', 4, 'acf-field', '', 0),
(22, 1, '2017-02-06 09:13:55', '2017-02-06 09:13:55', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Homepage Image', 'homepage_image', 'publish', 'closed', 'closed', '', 'field_58983e151b081', '', '', '2017-02-06 09:13:55', '2017-02-06 09:13:55', '', 21, 'http://workerassist.dev/?post_type=acf-field&p=22', 0, 'acf-field', '', 0),
(23, 1, '2017-02-06 09:16:41', '2017-02-06 09:16:41', 'a:7:{s:9:"placement";s:3:"top";s:8:"endpoint";i:0;s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Homepage Slider', '', 'publish', 'closed', 'closed', '', 'field_58983e61322e3', '', '', '2017-02-08 03:32:27', '2017-02-08 03:32:27', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=23', 0, 'acf-field', '', 0),
(24, 1, '2017-02-06 09:16:41', '2017-02-06 09:16:41', 'a:7:{s:9:"placement";s:3:"top";s:8:"endpoint";i:0;s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Resource Banner', 'resource_banner', 'publish', 'closed', 'closed', '', 'field_58983ee5322e4', '', '', '2017-02-08 03:35:48', '2017-02-08 03:35:48', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=24', 5, 'acf-field', '', 0),
(25, 1, '2017-02-06 09:44:23', '2017-02-06 09:44:23', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58983dc91b07e";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}}}', 'Resource Banner Image', 'resource_banner_image', 'publish', 'closed', 'closed', '', 'field_58984557e4e65', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=25', 9, 'acf-field', '', 0),
(26, 1, '2017-02-06 09:44:23', '2017-02-06 09:44:23', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Resource Image', 'resource_image', 'publish', 'closed', 'closed', '', 'field_58984557e4e66', '', '', '2017-02-06 09:44:23', '2017-02-06 09:44:23', '', 25, 'http://workerassist.dev/?post_type=acf-field&p=26', 0, 'acf-field', '', 0),
(27, 1, '2017-02-06 09:46:35', '2017-02-06 09:46:35', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58983de01b07f";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}}}', 'News Banner Image', 'news_banner_image', 'publish', 'closed', 'closed', '', 'field_589845e7bae59', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=27', 14, 'acf-field', '', 0),
(28, 1, '2017-02-06 09:46:35', '2017-02-06 09:46:35', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'News Image', 'resource_image', 'publish', 'closed', 'closed', '', 'field_589845e7bae5a', '', '', '2017-02-06 09:46:35', '2017-02-06 09:46:35', '', 27, 'http://workerassist.dev/?post_type=acf-field&p=28', 0, 'acf-field', '', 0),
(29, 1, '2017-02-06 09:46:35', '2017-02-06 09:46:35', 'a:7:{s:9:"placement";s:3:"top";s:8:"endpoint";i:0;s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'News Banner', 'news_banner', 'publish', 'closed', 'closed', '', 'field_589845c1bae57', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=29', 10, 'acf-field', '', 0),
(30, 1, '2017-02-06 09:50:59', '2017-02-06 09:50:59', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58983d871b07c";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}}}', 'Next Step Image', 'resource_next_step_image', 'publish', 'closed', 'closed', '', 'field_589846e602cd7', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=30', 7, 'acf-field', '', 0),
(31, 1, '2017-02-06 10:09:27', '2017-02-06 10:09:27', '', 'next-step', '', 'inherit', 'open', 'closed', '', 'next-step', '', '', '2017-02-08 11:10:25', '2017-02-08 11:10:25', '', 0, 'http://workerassist.dev/wp-content/uploads/2017/02/next-step.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2017-02-06 10:09:36', '2017-02-06 10:09:36', '', 'banner1', '', 'inherit', 'open', 'closed', '', 'banner1', '', '', '2017-02-06 10:10:03', '2017-02-06 10:10:03', '', 0, 'http://workerassist.dev/wp-content/uploads/2017/02/banner1.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2017-02-06 11:06:05', '2017-02-06 11:06:05', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:22:"acf-options-what-we-do";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'What We Do', 'what-we-do', 'publish', 'closed', 'closed', '', 'group_589858814fc1e', '', '', '2017-02-06 12:26:26', '2017-02-06 12:26:26', '', 0, 'http://workerassist.dev/?post_type=acf-field-group&#038;p=33', 0, 'acf-field-group', '', 0),
(34, 1, '2017-02-06 11:06:05', '2017-02-06 11:06:05', 'a:10:{s:13:"default_value";s:0:"";s:9:"maxlength";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Intro Title', 'whatwedo_intro_title', 'publish', 'closed', 'closed', '', 'field_589858876e1c0', '', '', '2017-02-06 11:06:05', '2017-02-06 11:06:05', '', 33, 'http://workerassist.dev/?post_type=acf-field&p=34', 0, 'acf-field', '', 0),
(35, 1, '2017-02-06 11:06:30', '2017-02-06 11:06:30', 'a:10:{s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;s:13:"default_value";s:0:"";s:5:"delay";i:0;s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Intro Text', 'whatwedo_intro_text', 'publish', 'closed', 'closed', '', 'field_589858ab48a16', '', '', '2017-02-06 12:23:58', '2017-02-06 12:23:58', '', 33, 'http://workerassist.dev/?post_type=acf-field&#038;p=35', 1, 'acf-field', '', 0),
(36, 1, '2017-02-06 11:15:11', '2017-02-06 11:15:11', 'a:10:{s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";s:9:"collapsed";s:0:"";s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Services', 'whatwedo_services', 'publish', 'closed', 'closed', '', 'field_589858eda289b', '', '', '2017-02-06 12:23:00', '2017-02-06 12:23:00', '', 33, 'http://workerassist.dev/?post_type=acf-field&#038;p=36', 2, 'acf-field', '', 0),
(37, 1, '2017-02-06 11:15:11', '2017-02-06 11:15:11', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Image', 'service_image', 'publish', 'closed', 'closed', '', 'field_58985a5aa289c', '', '', '2017-02-06 11:15:11', '2017-02-06 11:15:11', '', 36, 'http://workerassist.dev/?post_type=acf-field&p=37', 0, 'acf-field', '', 0),
(38, 1, '2017-02-06 11:15:11', '2017-02-06 11:15:11', 'a:10:{s:13:"default_value";s:0:"";s:9:"maxlength";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Title', 'service_title', 'publish', 'closed', 'closed', '', 'field_58985a7ba289d', '', '', '2017-02-06 11:15:11', '2017-02-06 11:15:11', '', 36, 'http://workerassist.dev/?post_type=acf-field&p=38', 1, 'acf-field', '', 0),
(39, 1, '2017-02-06 11:15:11', '2017-02-06 11:15:11', 'a:10:{s:13:"default_value";s:0:"";s:9:"new_lines";s:7:"wpautop";s:9:"maxlength";s:0:"";s:11:"placeholder";s:0:"";s:4:"rows";s:0:"";s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Description', 'service_description', 'publish', 'closed', 'closed', '', 'field_58985a85a289e', '', '', '2017-02-06 12:26:26', '2017-02-06 12:26:26', '', 36, 'http://workerassist.dev/?post_type=acf-field&#038;p=39', 2, 'acf-field', '', 0),
(40, 1, '2017-02-06 11:37:36', '2017-02-06 11:37:36', '', 'Gemma', '', 'publish', 'closed', 'closed', '', 'gemma', '', '', '2017-02-06 11:47:56', '2017-02-06 11:47:56', '', 0, 'http://workerassist.dev/?post_type=casestudies&#038;p=40', 0, 'casestudies', '', 0),
(41, 1, '2017-02-06 11:33:54', '2017-02-06 11:33:54', '', 'casestudy_lin', '', 'inherit', 'open', 'closed', '', 'casestudy_lin', '', '', '2017-02-06 11:51:12', '2017-02-06 11:51:12', '', 40, 'http://workerassist.dev/wp-content/uploads/2017/02/casestudy_lin.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2017-02-06 11:33:54', '2017-02-06 11:33:54', '', 'casestudy_martin', '', 'inherit', 'open', 'closed', '', 'casestudy_martin', '', '', '2017-02-06 11:48:14', '2017-02-06 11:48:14', '', 40, 'http://workerassist.dev/wp-content/uploads/2017/02/casestudy_martin.jpg', 0, 'attachment', 'image/jpeg', 0),
(43, 1, '2017-02-06 11:33:55', '2017-02-06 11:33:55', '', 'casestudy_gemma', '', 'inherit', 'open', 'closed', '', 'casestudy_gemma', '', '', '2017-02-06 11:34:31', '2017-02-06 11:34:31', '', 40, 'http://workerassist.dev/wp-content/uploads/2017/02/casestudy_gemma.jpg', 0, 'attachment', 'image/jpeg', 0),
(44, 1, '2017-02-06 11:33:55', '2017-02-06 11:33:55', '', 'image-service3', '', 'inherit', 'open', 'closed', '', 'image-service3', '', '', '2017-02-06 12:28:50', '2017-02-06 12:28:50', '', 40, 'http://workerassist.dev/wp-content/uploads/2017/02/image-service3.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2017-02-06 11:33:56', '2017-02-06 11:33:56', '', 'image-service2', '', 'inherit', 'open', 'closed', '', 'image-service2', '', '', '2017-02-06 12:27:56', '2017-02-06 12:27:56', '', 40, 'http://workerassist.dev/wp-content/uploads/2017/02/image-service2.jpg', 0, 'attachment', 'image/jpeg', 0),
(46, 1, '2017-02-06 11:33:56', '2017-02-06 11:33:56', '', 'image-service1', '', 'inherit', 'open', 'closed', '', 'image-service1', '', '', '2017-02-06 12:25:56', '2017-02-06 12:25:56', '', 40, 'http://workerassist.dev/wp-content/uploads/2017/02/image-service1.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2017-02-06 11:49:23', '2017-02-06 11:49:23', '', 'Martin', '', 'publish', 'closed', 'closed', '', 'martin', '', '', '2017-02-06 12:12:16', '2017-02-06 12:12:16', '', 0, 'http://workerassist.dev/?post_type=casestudies&#038;p=47', 0, 'casestudies', '', 0),
(48, 1, '2017-02-06 11:51:01', '2017-02-06 11:51:01', '', 'Lin', '', 'publish', 'closed', 'closed', '', 'lin', '', '', '2017-02-06 12:11:58', '2017-02-06 12:11:58', '', 0, 'http://workerassist.dev/?post_type=casestudies&#038;p=48', 0, 'casestudies', '', 0),
(49, 1, '2017-02-06 22:24:40', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-02-06 22:24:40', '0000-00-00 00:00:00', '', 0, 'http://workerassist.dev/?post_type=resources&p=49', 0, 'resources', '', 0),
(50, 1, '2017-02-06 22:25:15', '2017-02-06 22:25:15', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:9:"resources";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Resources', 'resources', 'publish', 'closed', 'closed', '', 'group_5898f7b0c900b', '', '', '2017-02-06 22:25:47', '2017-02-06 22:25:47', '', 0, 'http://workerassist.dev/?post_type=acf-field-group&#038;p=50', 0, 'acf-field-group', '', 0),
(51, 1, '2017-02-06 22:25:15', '2017-02-06 22:25:15', 'a:12:{s:13:"default_value";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:4:"step";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:4:"type";s:6:"number";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Print Count', 'print_count', 'publish', 'closed', 'closed', '', 'field_5898f7b9e8077', '', '', '2017-02-06 22:25:15', '2017-02-06 22:25:15', '', 50, 'http://workerassist.dev/?post_type=acf-field&p=51', 0, 'acf-field', '', 0),
(52, 1, '2017-02-06 22:25:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-02-06 22:25:26', '0000-00-00 00:00:00', '', 0, 'http://workerassist.dev/?post_type=resources&p=52', 0, 'resources', '', 0),
(53, 1, '2017-02-06 23:18:20', '2017-02-06 23:18:20', 'Please note, the information provided here is intended as a guide only for claims under the Tasmanian Workers Rehabilitation and Compensation Scheme and is not to be used as legal advice. For further and more detailed information, specific legal advice or assistance about your matter, please contact us.\r\n\r\n<strong>What are my entitlements if i have an accepted workers compensation claim?</strong>\r\nThe Workers Compensation Act provides for 3 different entitlements if you have an accepted claim:\r\n<ul>\r\n<li>Weekly payments of compensation</li>\r\n<li>Reasonable medical expenses</li>\r\n<li>Lump sum payment for whole person impairment of 5% or more for physical injuries, or 10% or more for psychological injuries.</li>\r\n</ul>\r\n\r\n\r\n<strong>\r\nCommon Law Rights</strong>\r\nAdditionally, if you have suffered your injury as a result of the negligence of someone else, you may have rights to damages at common law.\r\nIf the negligent party was your employer, under the current legislation, you must achieve at least a 20% whole person impairment before you have the right to sue for damages.\r\nThere are strict time limits which apply if you intend to sue for damages. We recommend that you seek legal advice as soon as possible to receive specific advice about this issue.\r\n', 'Workers Compensation Entitlements', '', 'publish', 'closed', 'closed', '', 'workers-compensation-entitlements', '', '', '2017-02-06 23:18:20', '2017-02-06 23:18:20', '', 0, 'http://workerassist.dev/?post_type=resources&#038;p=53', 0, 'resources', '', 0),
(54, 1, '2017-02-06 23:21:33', '2017-02-06 23:21:33', '<em>Please note, the information provided here is intended as a guide only for claims under the Tasmanian Workers Rehabilitation and Compensation Scheme and is not to be used as legal advice. For further and more detailed information, specific legal advice or assistance about your matter, please contact us.</em>\r\n\r\n<strong>What happens when I am ready to return to work?</strong>\r\nIf you have had to take time off work as a result of an injury at work, the workers compensation system is designed to assist you in returning to work as soon as possible. Evidence shows that the sooner you are able to return to work, the better chance you have of a good outcome generally from the workers compensation process.\r\n\r\nIdeally, the aim of the Tasmanian Workers Compensation system is to return you to your job, with your employer.\r\n\r\nYour primary treating medical practitioner (GP) determines whether you are fit for work based on your medical condition.\r\n\r\n<strong>Your Role</strong>\r\nYou should be an active participant in the return to work process.\r\n\r\nIt is important to assist your GP by identifying duties that you think you can do, even if they are less demanding than or different to your normal duties. A gradual return to work is better than no return to work for your long term health.\r\n\r\nBe in contact with your employer about the progress of injury and work with them to identify jobs at work you think you can do within any restrictions that you have. Or, if this is not possible, work with your employer to try and modify tasks so that you can do some duties.\r\n\r\nYou should comply with medical restrictions placed on you by your GP.\r\n\r\n<strong>Return to Work and Injury Management Plans</strong>\r\nFor injured workers who are likely to be incapacitated for 5 days or more, a Return to Work Plan must be completed, in order to assist your re-introduction into the workplace. A Return to Work Plan is a simple plan for co-ordinating and managing treatment, rehabilitation and return to work.\r\n\r\nFor injured workers who are likely to be incapacitated for more than 28 days, a more complex plan called an Injury Management Plan is developed.\r\n\r\nSuch plans should be developed, as far as possible, in consultation with:\r\n<ul>\r\n 	<li>You, the injured worker</li>\r\n 	<li>Your employer</li>\r\n 	<li>Your GP</li>\r\n 	<li>Your insurer (or self insurer)</li>\r\n 	<li>Your workplace rehabilitation provider; and</li>\r\n 	<li>Your injury management co-ordinator</li>\r\n 	<li>Plans are to be regularly reviewed.</li>\r\n</ul>', 'Returning to Work following a Workers Compensation Claim', '', 'publish', 'closed', 'closed', '', 'returning-to-work-following-a-workers-compensation-claim', '', '', '2017-02-06 23:21:33', '2017-02-06 23:21:33', '', 0, 'http://workerassist.dev/?post_type=resources&#038;p=54', 0, 'resources', '', 0),
(55, 1, '2017-02-06 23:23:40', '2017-02-06 23:23:40', '<em>Please note, the information provided here is intended as a guide only for claims under the Tasmanian Workers Rehabilitation and Compensation Scheme and is not to be used as legal advice. For further and more detailed information, specific legal advice or assistance about your matter, please contact us.</em>\r\n\r\n<strong>Notice</strong>\r\nThe first thing you must do if you are an injured worker is to give notice of your injury or possible injury. If you are in doubt about whether you have in fact been injured, or whether you will need medical treatment, it is best to err on the side of caution and give notice of your injury or possible injury to your employer. Giving notice as soon as practicable is very important.\r\n\r\nTo give notice, you must advise your employer of:\r\n<ul>\r\n 	<li>what your injury is;</li>\r\n 	<li>the cause of your injury; and</li>\r\n 	<li>when it happened.</li>\r\n</ul>\r\nIt is best to record this notice in writing.\r\n\r\n<strong>Lodging the claim with your employer</strong>\r\nIf you need to seek medical treatment for your injury, or need time off work, you must visit your treating medical practitioner (GP). If the GP considers that you have suffered from a work related injury, he or she will issue you with a workers compensation medical certificate. You must lodge this certificate with your employer.\r\n\r\nYou also need to lodge a workers compensation claim form. You can obtain this from your employer. Once you have completed it, return it and lodge it with your employer.\r\n\r\nBoth the workers compensation medical certificate and claim form must be lodged with your employer in order for you to have completed your claim.\r\n\r\n<strong>Time Limits</strong>\r\nA claim for compensation must be lodged within 6 months of the occurrence of the injury (or death).', 'Lodging a claim for Workers Compensation', '', 'publish', 'closed', 'closed', '', 'lodging-a-claim-for-workers-compensation', '', '', '2017-02-06 23:23:54', '2017-02-06 23:23:54', '', 0, 'http://workerassist.dev/?post_type=resources&#038;p=55', 0, 'resources', '', 0),
(56, 1, '2017-02-06 23:23:46', '2017-02-06 23:23:46', '<em>Please note, the information provided here is intended as a guide only for claims under the Tasmanian Workers Rehabilitation and Compensation Scheme and is not to be used as legal advice. For further and more detailed information, specific legal advice or assistance about your matter, please contact us.</em>\n\n<strong>Notice</strong>\nThe first thing you must do if you are an injured worker is to give notice of your injury or possible injury. If you are in doubt about whether you have in fact been injured, or whether you will need medical treatment, it is best to err on the side of caution and give notice of your injury or possible injury to your employer. Giving notice as soon as practicable is very important.\n\nTo give notice, you must advise your employer of:\n<ul>\n 	<li>what your injury is;</li>\n 	<li>the cause of your injury; and</li>\n 	<li>when it happened.</li>\n</ul>\nIt is best to record this notice in writing.\n\n<strong>Lodging the claim with your employer</strong>\nIf you need to seek medical treatment for your injury, or need time off work, you must visit your treating medical practitioner (GP). If the GP considers that you have suffered from a work related injury, he or she will issue you with a workers compensation medical certificate. You must lodge this certificate with your employer.\n\nYou also need to lodge a workers compensation claim form. You can obtain this from your employer. Once you have completed it, return it and lodge it with your employer.\n\nBoth the workers compensation medical certificate and claim form must be lodged with your employer in order for you to have completed your claim.\n\n<strong>Time Limits</strong>\nA claim for compensation must be lodged within 6 months of the occurrence of the injury (or death).', 'Lodging a claim for Workers Compensation', '', 'inherit', 'closed', 'closed', '', '55-autosave-v1', '', '', '2017-02-06 23:23:46', '2017-02-06 23:23:46', '', 55, 'http://workerassist.dev/55-autosave-v1/', 0, 'revision', '', 0),
(57, 1, '2017-02-06 23:25:33', '2017-02-06 23:25:33', '<em>Please note, the information provided here is intended as a guide only for claims under the Tasmanian Workers Rehabilitation and Compensation Scheme and is not to be used as legal advice. For further and more detailed information, specific legal advice or assistance about your matter, please contact us.</em>\r\n\r\n<strong>What happens when I make my claim for workers compensation?</strong>\r\nYour employer has 84 days within which to either accept or dispute your claim. During this period of time, you are entitled to receive without prejudice weekly payments of compensation and the payment of medical expenses up to an amount of $5,000.\r\n<strong>\r\nWhat happens if my claim is disputed?</strong>\r\nYour matter will be referred to the Workers Rehabilitation and Compensation Tribunal for determination by a Commissioner. If your matter is disputed, you will receive formal notification in writing.\r\n\r\n<strong>Do I have to talk to a factual investigator?</strong>\r\nNo, you do not have to speak to a factual investigator who has been engaged by your employer/insurer. Whether you give a factual statement is entirely a matter for you.\r\n\r\n<strong>Do I have to attend an independent medical examination?</strong>\r\nYes, you do. Your employer/insurer is entitled to have you examined once every 3 months per specialist type. The circumstances of the examination, such as time, place and notice must be reasonable.\r\n\r\n<strong>Can I see my own primary treating medical practitioner (GP)?</strong>\r\nYes. You are entitled to choose your own GP.\r\n\r\n<strong>Can I change GP’s?</strong>\r\nYes, you can. If you decide to do so, you must:\r\n<ul>\r\n 	<li>Advise your employer of the name of your GP; and</li>\r\n 	<li>Authorise the transfer of your medical records to your new GP</li>\r\n</ul>\r\n<strong>Is anyone else entitled to attend GP appointments with me?</strong>\r\nNo, not as a matter of right. It is entirely up to you as the injured worker whether you permit others to attend GP appointments with you.', 'Frequently Asked Questions', '', 'publish', 'closed', 'closed', '', 'frequently-asked-questions', '', '', '2017-02-07 02:19:46', '2017-02-07 02:19:46', '', 0, 'http://workerassist.dev/?post_type=resources&#038;p=57', 0, 'resources', '', 0),
(58, 1, '2017-02-06 23:41:01', '2017-02-06 23:41:01', 'Learn about the workers compensation claim process, workers rights, dispute resolution, injury management and return-to-work at the upcoming HSR training seminars. To register, contact Worker Assist on 1300 027 747 or fill out our application form.\r\n<hr>\r\n\r\n<strong>APPLICATION FORM </strong>\r\nNam andicia tempost otatem re plabo. Am eaquiandem re corum que dolore voluptios dem. Obit, volorpo restrum excepra tentior istiaspedis voloren iassequ amentiis a commodipid quisti quo explaceped eatur, nihillenis et laut omnietur, con es enditia tusciis arum quo blab illenda ntiorempe de nest quas nis et est, nostior iorpor moloritatem qui aute corum, corro ipsam, aut quam etur, con nim aceri quae et hitem experfe rumque volora non nulliam, voloreh enient.\r\n', 'Training for Health and Safety Representatives', 'Learn about the workers compensation claim process, workers rights, dispute resolution, injury management and return-to-work at the upcoming HSR training seminars. To register, contact Worker Assist on 1300 027 747 or fill out our application form. ', 'publish', 'open', 'open', '', 'training-for-health-and-safety-representatives', '', '', '2017-02-06 23:48:23', '2017-02-06 23:48:23', '', 0, 'http://workerassist.dev/?p=58', 0, 'post', '', 0),
(59, 1, '2017-02-06 23:41:01', '2017-02-06 23:41:01', 'Learn about the workers compensation claim process, workers rights, dispute resolution, injury management and return-to-work at the upcoming HSR training seminars. To register, contact Worker Assist on 1300 027 747 or fill out our application form.\r\n<hr>\r\n\r\n<strong>APPLICATION FORM </strong>\r\nNam andicia tempost otatem re plabo. Am eaquiandem re corum que dolore voluptios dem. Obit, volorpo restrum excepra tentior istiaspedis voloren iassequ amentiis a commodipid quisti quo explaceped eatur, nihillenis et laut omnietur, con es enditia tusciis arum quo blab illenda ntiorempe de nest quas nis et est, nostior iorpor moloritatem qui aute corum, corro ipsam, aut quam etur, con nim aceri quae et hitem experfe rumque volora non nulliam, voloreh enient.\r\n', 'Training for Health and Safety Representatives', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2017-02-06 23:41:01', '2017-02-06 23:41:01', '', 58, 'http://workerassist.dev/58-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2017-02-06 23:47:25', '2017-02-06 23:47:25', '', 'Workers Compensation in the workplace', 'As part of WorkSafe month, Worker Assist will be offering a free seminar on workers compensation matters in the workplace during \r\nOctober 2017. Details will be available soon. ', 'publish', 'open', 'open', '', 'workers-compensation-in-the-workplace', '', '', '2017-02-06 23:47:25', '2017-02-06 23:47:25', '', 0, 'http://workerassist.dev/?p=60', 0, 'post', '', 0),
(61, 1, '2017-02-06 23:47:25', '2017-02-06 23:47:25', '', 'Workers Compensation in the workplace', 'As part of WorkSafe month, Worker Assist will be offering a free seminar on workers compensation matters in the workplace during \r\nOctober 2017. Details will be available soon. ', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2017-02-06 23:47:25', '2017-02-06 23:47:25', '', 60, 'http://workerassist.dev/60-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2017-02-06 23:48:17', '2017-02-06 23:48:17', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-02-06 23:48:17', '2017-02-06 23:48:17', '', 1, 'http://workerassist.dev/1-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2017-02-06 23:48:23', '2017-02-06 23:48:23', 'Learn about the workers compensation claim process, workers rights, dispute resolution, injury management and return-to-work at the upcoming HSR training seminars. To register, contact Worker Assist on 1300 027 747 or fill out our application form.\r\n<hr>\r\n\r\n<strong>APPLICATION FORM </strong>\r\nNam andicia tempost otatem re plabo. Am eaquiandem re corum que dolore voluptios dem. Obit, volorpo restrum excepra tentior istiaspedis voloren iassequ amentiis a commodipid quisti quo explaceped eatur, nihillenis et laut omnietur, con es enditia tusciis arum quo blab illenda ntiorempe de nest quas nis et est, nostior iorpor moloritatem qui aute corum, corro ipsam, aut quam etur, con nim aceri quae et hitem experfe rumque volora non nulliam, voloreh enient.\r\n', 'Training for Health and Safety Representatives', 'Learn about the workers compensation claim process, workers rights, dispute resolution, injury management and return-to-work at the upcoming HSR training seminars. To register, contact Worker Assist on 1300 027 747 or fill out our application form. ', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2017-02-06 23:48:23', '2017-02-06 23:48:23', '', 58, 'http://workerassist.dev/58-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2017-02-07 00:06:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2017-02-07 00:06:02', '0000-00-00 00:00:00', '', 0, 'http://workerassist.dev/?p=64', 0, 'post', '', 0),
(65, 1, '2017-02-07 00:07:14', '2017-02-07 00:07:14', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:24:"acf-options-useful-links";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Useful Links', 'useful-links', 'publish', 'closed', 'closed', '', 'group_58990f6f34d87', '', '', '2017-02-07 00:07:29', '2017-02-07 00:07:29', '', 0, 'http://workerassist.dev/?post_type=acf-field-group&#038;p=65', 0, 'acf-field-group', '', 0),
(66, 1, '2017-02-07 00:07:14', '2017-02-07 00:07:14', 'a:10:{s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";s:9:"collapsed";s:0:"";s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Links', 'links', 'publish', 'closed', 'closed', '', 'field_58990f7009b0d', '', '', '2017-02-07 00:07:14', '2017-02-07 00:07:14', '', 65, 'http://workerassist.dev/?post_type=acf-field&p=66', 0, 'acf-field', '', 0),
(67, 1, '2017-02-07 00:07:14', '2017-02-07 00:07:14', 'a:10:{s:13:"default_value";s:0:"";s:9:"maxlength";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Title', 'link_title', 'publish', 'closed', 'closed', '', 'field_58990f7f09b0e', '', '', '2017-02-07 00:07:14', '2017-02-07 00:07:14', '', 66, 'http://workerassist.dev/?post_type=acf-field&p=67', 0, 'acf-field', '', 0),
(68, 1, '2017-02-07 00:07:14', '2017-02-07 00:07:14', 'a:10:{s:13:"default_value";s:0:"";s:9:"maxlength";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'URL', 'link_url', 'publish', 'closed', 'closed', '', 'field_58990f9509b0f', '', '', '2017-02-07 00:07:14', '2017-02-07 00:07:14', '', 66, 'http://workerassist.dev/?post_type=acf-field&p=68', 1, 'acf-field', '', 0),
(70, 1, '2017-02-07 00:42:50', '2017-02-07 00:42:50', '', 'banner3', '', 'inherit', 'open', 'closed', '', 'banner3', '', '', '2017-02-07 00:46:26', '2017-02-07 00:46:26', '', 0, 'http://workerassist.dev/wp-content/uploads/2017/02/banner3.jpg', 0, 'attachment', 'image/jpeg', 0),
(71, 1, '2017-02-07 00:52:15', '2017-02-07 00:52:15', '', 'banner4', '', 'inherit', 'open', 'closed', '', 'banner4', '', '', '2017-02-07 00:52:17', '2017-02-07 00:52:17', '', 0, 'http://workerassist.dev/wp-content/uploads/2017/02/banner4.jpg', 0, 'attachment', 'image/jpeg', 0),
(73, 1, '2017-02-08 01:01:52', '2017-02-08 01:01:52', '', 'Print Template', '', 'publish', 'closed', 'closed', '', 'print-template', '', '', '2017-02-08 01:02:41', '2017-02-08 01:02:41', '', 0, 'http://workerassist.dev/?page_id=73', 0, 'page', '', 0),
(74, 1, '2017-02-08 01:01:52', '2017-02-08 01:01:52', '', 'Print Template', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2017-02-08 01:01:52', '2017-02-08 01:01:52', '', 73, 'http://workerassist.dev/73-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2017-02-08 03:35:48', '2017-02-08 03:35:48', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58983d871b07c";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}}}', 'Next Step Image', 'homepage_next_step_image', 'publish', 'closed', 'closed', '', 'field_589a91bda3c4b', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=75', 2, 'acf-field', '', 0),
(76, 1, '2017-02-08 03:35:48', '2017-02-08 03:35:48', 'a:10:{s:13:"default_value";i:0;s:7:"message";s:0:"";s:2:"ui";i:0;s:10:"ui_on_text";s:0:"";s:11:"ui_off_text";s:0:"";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Show Next Step Graphic', 'resource_show_next_step_graphic', 'publish', 'closed', 'closed', '', 'field_589a9185a3c4a', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&#038;p=76', 6, 'acf-field', '', 0),
(77, 1, '2017-02-08 03:36:58', '2017-02-08 03:36:58', 'a:10:{s:13:"default_value";i:0;s:7:"message";s:0:"";s:2:"ui";i:0;s:10:"ui_on_text";s:0:"";s:11:"ui_off_text";s:0:"";s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}}', 'Show Next Step Graphic', 'news_show_next_step_graphic', 'publish', 'closed', 'closed', '', 'field_589a9218b42d8', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&p=77', 11, 'acf-field', '', 0),
(78, 1, '2017-02-08 03:36:58', '2017-02-08 03:36:58', 'a:15:{s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58983d871b07c";s:8:"operator";s:2:"==";s:5:"value";s:1:"1";}}}}', 'Next Step Image', 'news_next_step_image', 'publish', 'closed', 'closed', '', 'field_589a922fb42d9', '', '', '2017-02-08 03:36:58', '2017-02-08 03:36:58', '', 16, 'http://workerassist.dev/?post_type=acf-field&p=78', 12, 'acf-field', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_form`
--

CREATE TABLE `wp_rg_form` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rg_form`
--

INSERT INTO `wp_rg_form` (`id`, `title`, `date_created`, `is_active`, `is_trash`) VALUES
(1, 'Contact Form', '2017-02-07 05:54:45', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_form_meta`
--

CREATE TABLE `wp_rg_form_meta` (
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `display_meta` longtext,
  `entries_grid_meta` longtext,
  `confirmations` longtext,
  `notifications` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rg_form_meta`
--

INSERT INTO `wp_rg_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`) VALUES
(1, '{"title":"Contact Form","description":"","labelPlacement":"top_label","descriptionPlacement":"below","button":{"type":"text","text":"Return Call","imageUrl":""},"fields":[{"type":"text","id":1,"label":"Name","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"Please fill out your name.","inputs":null,"formId":1,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"Name","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"pageNumber":1,"displayOnly":""},{"type":"text","id":2,"label":"Phone","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":1,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"Phone","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"pageNumber":1,"displayOnly":""},{"type":"text","id":3,"label":"Email","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":1,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"Email","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"pageNumber":1,"displayOnly":""}],"version":"2.1.2","id":1,"useCurrentUserAsAuthor":true,"postContentTemplateEnabled":false,"postTitleTemplateEnabled":false,"postTitleTemplate":"","postContentTemplate":"","lastPageButton":null,"pagination":null,"firstPageCssClass":null,"subLabelPlacement":"below","cssClass":"form","enableHoneypot":false,"enableAnimation":false,"save":{"enabled":false,"button":{"type":"link","text":"Save and Continue Later"}},"limitEntries":false,"limitEntriesCount":"","limitEntriesPeriod":"","limitEntriesMessage":"","scheduleForm":false,"scheduleStart":"","scheduleStartHour":"","scheduleStartMinute":"","scheduleStartAmpm":"","scheduleEnd":"","scheduleEndHour":"","scheduleEndMinute":"","scheduleEndAmpm":"","schedulePendingMessage":"","scheduleMessage":"","requireLogin":false,"requireLoginMessage":""}', NULL, '{"58996125d8f39":{"id":"58996125d8f39","name":"Default Confirmation","isDefault":true,"type":"message","message":"Thanks for contacting us! We will get in touch with you shortly.","url":"","pageId":"","queryString":""}}', '{"58996125d8a06":{"id":"58996125d8a06","to":"{admin_email}","name":"Admin Notification","event":"form_submission","toType":"email","subject":"New submission from {form_title}","message":"{all_fields}"}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_form_view`
--

CREATE TABLE `wp_rg_form_view` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) DEFAULT NULL,
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rg_form_view`
--

INSERT INTO `wp_rg_form_view` (`id`, `form_id`, `date_created`, `ip`, `count`) VALUES
(1, 1, '2017-02-07 09:27:58', '127.0.0.1\0\0\0\0\0\0', 286),
(2, 1, '2017-02-08 09:32:05', '127.0.0.1', 266);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_incomplete_submissions`
--

CREATE TABLE `wp_rg_incomplete_submissions` (
  `uuid` char(32) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) DEFAULT NULL,
  `source_url` longtext,
  `submission` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead`
--

CREATE TABLE `wp_rg_lead` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) DEFAULT NULL,
  `source_url` varchar(200) DEFAULT NULL,
  `user_agent` varchar(250) DEFAULT NULL,
  `currency` varchar(5) DEFAULT NULL,
  `payment_status` varchar(15) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rg_lead`
--

INSERT INTO `wp_rg_lead` (`id`, `form_id`, `post_id`, `date_created`, `is_starred`, `is_read`, `ip`, `source_url`, `user_agent`, `currency`, `payment_status`, `payment_date`, `payment_amount`, `payment_method`, `transaction_id`, `is_fulfilled`, `created_by`, `transaction_type`, `status`) VALUES
(1, 1, NULL, '2017-02-07 06:44:46', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(2, 1, NULL, '2017-02-07 06:45:58', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(3, 1, NULL, '2017-02-07 06:45:58', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(4, 1, NULL, '2017-02-07 06:46:02', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(5, 1, NULL, '2017-02-07 06:46:07', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(6, 1, NULL, '2017-02-07 06:46:11', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(7, 1, NULL, '2017-02-07 06:46:11', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(8, 1, NULL, '2017-02-07 06:46:12', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(9, 1, NULL, '2017-02-07 06:46:12', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(10, 1, NULL, '2017-02-07 06:46:16', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(11, 1, NULL, '2017-02-07 06:46:21', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(12, 1, NULL, '2017-02-07 06:46:24', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(13, 1, NULL, '2017-02-07 06:46:24', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(14, 1, NULL, '2017-02-07 06:46:25', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(15, 1, NULL, '2017-02-07 06:46:57', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(16, 1, NULL, '2017-02-07 06:47:08', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(17, 1, NULL, '2017-02-07 06:47:43', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(18, 1, NULL, '2017-02-07 06:47:53', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(19, 1, NULL, '2017-02-07 07:18:31', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash'),
(20, 1, NULL, '2017-02-07 07:18:36', 0, 0, '127.0.0.1', 'http://workerassist.dev/', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', 'AUD', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'trash');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_detail`
--

CREATE TABLE `wp_rg_lead_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL,
  `field_number` float NOT NULL,
  `value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rg_lead_detail`
--

INSERT INTO `wp_rg_lead_detail` (`id`, `lead_id`, `form_id`, `field_number`, `value`) VALUES
(1, 1, 1, 1, 'Karlo Estrada'),
(2, 1, 1, 2, '0466903390'),
(3, 1, 1, 3, 'karloestrada@yahoo.com'),
(4, 2, 1, 1, 'Karlo Estrada'),
(5, 2, 1, 2, '0466903390'),
(6, 2, 1, 3, 'karloestrada@yahoo.com'),
(7, 3, 1, 1, 'Karlo Estrada'),
(8, 3, 1, 2, '0466903390'),
(9, 3, 1, 3, 'karloestrada@yahoo.com'),
(10, 4, 1, 1, 'Karlo Estrada'),
(11, 4, 1, 2, '0466903390'),
(12, 4, 1, 3, 'karloestrada@yahoo.com'),
(13, 5, 1, 1, 'Karlo Estrada'),
(14, 5, 1, 2, '0466903390'),
(15, 5, 1, 3, 'karloestrada@yahoo.com'),
(16, 6, 1, 1, 'Karlo Estrada'),
(17, 6, 1, 2, '0466903390'),
(18, 6, 1, 3, 'karloestrada@yahoo.com'),
(19, 7, 1, 1, 'Karlo Estrada'),
(20, 7, 1, 2, '0466903390'),
(21, 7, 1, 3, 'karloestrada@yahoo.com'),
(22, 8, 1, 1, 'Karlo Estrada'),
(23, 8, 1, 2, '0466903390'),
(24, 8, 1, 3, 'karloestrada@yahoo.com'),
(25, 9, 1, 1, 'Karlo Estrada'),
(26, 9, 1, 2, '0466903390'),
(27, 9, 1, 3, 'karloestrada@yahoo.com'),
(28, 10, 1, 1, 'Karlo Estrada'),
(29, 10, 1, 2, '0466903390'),
(30, 10, 1, 3, 'karloestrada@yahoo.com'),
(31, 11, 1, 1, 'Karlo Estrada'),
(32, 11, 1, 2, '0466903390'),
(33, 11, 1, 3, 'karloestrada@yahoo.com'),
(34, 12, 1, 1, 'Karlo Estrada'),
(35, 12, 1, 2, '0466903390'),
(36, 12, 1, 3, 'karloestrada@yahoo.com'),
(37, 13, 1, 1, 'Karlo Estrada'),
(38, 13, 1, 2, '0466903390'),
(39, 13, 1, 3, 'karloestrada@yahoo.com'),
(40, 14, 1, 1, 'Karlo Estrada'),
(41, 14, 1, 2, '0466903390'),
(42, 14, 1, 3, 'karloestrada@yahoo.com'),
(43, 15, 1, 1, 'Karlo Estrada'),
(44, 15, 1, 2, '0466903390'),
(45, 15, 1, 3, 'karloestrada@yahoo.com'),
(46, 16, 1, 1, 'Karlo Estrada'),
(47, 16, 1, 2, '0466903390'),
(48, 16, 1, 3, 'karloestrada@yahoo.com'),
(49, 17, 1, 3, 'df'),
(50, 18, 1, 1, 'Karlo Marcelo Estrada'),
(51, 18, 1, 2, '0466903390'),
(52, 18, 1, 3, 'karloestrada@gmail.com'),
(53, 19, 1, 3, 'sdf'),
(54, 20, 1, 3, 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_detail_long`
--

CREATE TABLE `wp_rg_lead_detail_long` (
  `lead_detail_id` bigint(20) UNSIGNED NOT NULL,
  `value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_meta`
--

CREATE TABLE `wp_rg_lead_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `lead_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rg_lead_notes`
--

CREATE TABLE `wp_rg_lead_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(250) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext,
  `note_type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(58, 1, 0),
(60, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) DEFAULT NULL,
  `description` longtext,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'karloestrada'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'false'),
(10, 1, 'locale', ''),
(11, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'wp_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '0'),
(15, 1, 'session_tokens', 'a:2:{s:64:"b238874280b7f626369db2d80337b1d827a225c60fed49d4c2a60315d6b0e915";a:4:{s:10:"expiration";i:1487057002;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36";s:5:"login";i:1485847402;}s:64:"b8b5d23e9664137273e4300cc35b68da65477168d2c9666f42cddd176123f4c6";a:4:{s:10:"expiration";i:1486724869;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1486552069;}}'),
(16, 1, 'wp_dashboard_quick_press_last_post_id', '64'),
(17, 1, 'acf_user_settings', 'a:0:{}'),
(18, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(19, 1, 'wp_user-settings-time', '1486424457'),
(20, 1, 'closedpostboxes_toplevel_page_theme-general-settings', 'a:0:{}'),
(21, 1, 'metaboxhidden_toplevel_page_theme-general-settings', 'a:0:{}'),
(22, 1, 'closedpostboxes_post', 'a:0:{}'),
(23, 1, 'metaboxhidden_post', 'a:10:{i:0;s:9:"formatdiv";i:1;s:23:"acf-group_58983d7cd9e38";i:2;s:23:"acf-group_5898133c817e2";i:3;s:23:"acf-group_5898f7b0c900b";i:4;s:23:"acf-group_589858814fc1e";i:5;s:13:"trackbacksdiv";i:6;s:10:"postcustom";i:7;s:16:"commentstatusdiv";i:8;s:7:"slugdiv";i:9;s:9:"authordiv";}'),
(24, 1, 'gform_recent_forms', 'a:1:{i:0;s:1:"1";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) DEFAULT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  `user_nicename` varchar(50) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_url` varchar(100) DEFAULT NULL,
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) DEFAULT NULL,
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'karloestrada', '$P$BOAIkHsi11Qe2Qeukl9D4uPUT..kYL1', 'karloestrada', 'karloestrada@yahoo.com', '', '2017-01-31 07:23:21', '', 0, 'karloestrada');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_rg_form`
--
ALTER TABLE `wp_rg_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rg_form_meta`
--
ALTER TABLE `wp_rg_form_meta`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `wp_rg_form_view`
--
ALTER TABLE `wp_rg_form_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_rg_incomplete_submissions`
--
ALTER TABLE `wp_rg_incomplete_submissions`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_rg_lead`
--
ALTER TABLE `wp_rg_lead`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `wp_rg_lead_detail`
--
ALTER TABLE `wp_rg_lead_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `lead_field_number` (`lead_id`,`field_number`),
  ADD KEY `lead_field_value` (`value`(191));

--
-- Indexes for table `wp_rg_lead_detail_long`
--
ALTER TABLE `wp_rg_lead_detail_long`
  ADD PRIMARY KEY (`lead_detail_id`);

--
-- Indexes for table `wp_rg_lead_meta`
--
ALTER TABLE `wp_rg_lead_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`(191)),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `form_id_meta_key` (`form_id`,`meta_key`(191));

--
-- Indexes for table `wp_rg_lead_notes`
--
ALTER TABLE `wp_rg_lead_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `lead_user_key` (`lead_id`,`user_id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=620;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `wp_rg_form`
--
ALTER TABLE `wp_rg_form`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_rg_form_view`
--
ALTER TABLE `wp_rg_form_view`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_rg_lead`
--
ALTER TABLE `wp_rg_lead`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `wp_rg_lead_detail`
--
ALTER TABLE `wp_rg_lead_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `wp_rg_lead_meta`
--
ALTER TABLE `wp_rg_lead_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_rg_lead_notes`
--
ALTER TABLE `wp_rg_lead_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
