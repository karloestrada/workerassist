<?php
/**
 * Custom Post Types
 *
 * @package starter
 */

function resources_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Resources', 'Post Type General Name', 'starter' ),
		'singular_name'       => _x( 'Resources', 'Post Type Singular Name', 'starter' ),
		'menu_name'           => __( 'Resources', 'starter' ),
		'parent_item_colon'   => __( 'Parent Resources', 'starter' ),
		'all_items'           => __( 'All Resources', 'starter' ),
		'view_item'           => __( 'View Resources', 'starter' ),
		'add_new_item'        => __( 'Add New Resources', 'starter' ),
		'add_new'             => __( 'Add New', 'starter' ),
		'edit_item'           => __( 'Edit Resources', 'starter' ),
		'update_item'         => __( 'Update Resources', 'starter' ),
		'search_items'        => __( 'Search Resources', 'starter' ),
		'not_found'           => __( 'Not Found', 'starter' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'starter' ),
	);
	
	// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'Resources', 'starter' ),
		'description'         => __( 'Resources', 'starter' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'tags' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'tags', 'page-attributes' )
	);
	
	// Registering your Custom Post Type
	register_post_type( 'resources', $args );

}

add_action( 'init', 'resources_custom_post_type', 0 );



function casestudies_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Case Studies', 'Post Type General Name', 'starter' ),
		'singular_name'       => _x( 'Case Studies', 'Post Type Singular Name', 'starter' ),
		'menu_name'           => __( 'Case Studies', 'starter' ),
		'parent_item_colon'   => __( 'Parent Case Studies', 'starter' ),
		'all_items'           => __( 'All Case Studies', 'starter' ),
		'view_item'           => __( 'View Case Studies', 'starter' ),
		'add_new_item'        => __( 'Add New Case Studies', 'starter' ),
		'add_new'             => __( 'Add New', 'starter' ),
		'edit_item'           => __( 'Edit Case Studies', 'starter' ),
		'update_item'         => __( 'Update Case Studies', 'starter' ),
		'search_items'        => __( 'Search Case Studies', 'starter' ),
		'not_found'           => __( 'Not Found', 'starter' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'starter' ),
	);
	
	// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'Case Studies', 'starter' ),
		'description'         => __( 'Case Studies', 'starter' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title' ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'tags' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'supports'           => array( 'title', 'editor', 'thumbnail', 'tags', 'page-attributes' )
	);
	
	// Registering your Custom Post Type
	register_post_type( 'casestudies', $args );

}

add_action( 'init', 'casestudies_custom_post_type', 0 );





?>