<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=100%, initial-scale=1.0, 
minimum-scale=1.0, maximum-scale=1.0, user-scalable=false">
	<meta name="HandheldFriendly" content="true">
	<meta name="theme-color" content="#ffffff">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/manifest.json">
	<link rel="mask-icon" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
	

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
	
	<script>
		var HOME_DIR = '<?php echo get_bloginfo( 'url' )?>';
		var THEME_DIR = '<?php echo get_bloginfo( 'template_directory' )?>'
	</script>

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?> id="body">

	<header class="site-header print">
		<div class="container">
			<div class="logo">
				<a href="<?php echo get_bloginfo( 'url' )?>"><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo.svg"></a>
			</div>  	
		</div>
	</header>




