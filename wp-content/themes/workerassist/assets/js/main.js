(function($) {

	$(document).ready(function() {

	
		//ON WINDOW RESIZE
		/*$(window).bind("debouncedresize", function() {
			var window_width = window.width;
			console.log(test);

		});*/

		$(window).on("debouncedresize", function( event ) {
		    var window_width = $(window).width();
		    //console.log(window_width);
		    if(window_width > 1279){
		    	$(".menu-mobile").removeClass("active");
		    	$(".casestudies-overlay").removeClass("active");
		    }
		});



		//MENU ITEM SCROLL TO SECTION #ANCHOR
		smoothScroll.init({
		    selector: '[data-scroll]', // Selector for links (must be a class, ID, data attribute, or element tag)
		    selectorHeader: null, // Selector for fixed headers (must be a valid CSS selector) [optional]
		    speed: 1000, // Integer. How fast to complete the scroll in milliseconds
		    easing: 'easeInOutCubic', // Easing pattern to use
		    offset: 70, // Integer. How far to offset the scrolling anchor location in pixels
		    callback: function ( anchor, toggle ) {} // Function to run after scrolling
		});

		if ( window.location.hash ) {
	        var anchor = document.querySelector( window.location.hash ); // Get the anchor
	        var toggle = document.querySelector( 'a[href*="' + window.location.hash + '"]' ); // Get the toggle (if one exists)
	        var options = {}; // Any custom options you want to use would go here
	        smoothScroll.animateScroll( anchor, toggle, options );
	    }

		$('.maincontent').on( 'click', '.scroll-contact', function(e) {
 			e.preventDefault();
 			var anchor = $(this).attr("href");
 			anchor = document.querySelector(anchor);
			smoothScroll.animateScroll( anchor );
 		});

 		$('.maincontent').on( 'click', '.resource-link', function(e) {
 			e.preventDefault();
 			var anchor = $(this).attr("href");
 			anchor = document.querySelector(anchor);
			smoothScroll.animateScroll( anchor );
 		});



		$('header').on( 'click', '.menu a', function(e) {
 			e.preventDefault();

 			var anchor = $(this).attr("href");

 			if($(this).hasClass("not-anchor")){
 				window.location.href = anchor;
 			}else if($('body').hasClass('home')){
 				anchor = document.querySelector(anchor);
				smoothScroll.animateScroll( anchor );
 			}else{
 				window.location.href = HOME_DIR + anchor;
 			}
 			
 		});

 		$('.menu-mobile').on( 'click', '.menu a', function(e) {
 			e.preventDefault();

 			$(".menu-mobile").toggleClass("active");
 			$(".menu-toggle").removeClass("close");

 			var anchor = $(this).attr("href");

 			if($(this).hasClass("not-anchor")){
 				window.location.href = anchor;
 			}else if($('body').hasClass('home')){
 				anchor = document.querySelector(anchor);
				smoothScroll.animateScroll( anchor );
 			}else{
 				window.location.href = HOME_DIR + anchor;
 			}
 			
 		});





		$(document).on( 'click', '.menu-toggle', function(e) {
			e.preventDefault();
		    $(".menu-mobile").toggleClass("active");
		    $("body").toggleClass("mobile-active");
		    $(this).toggleClass("close");
		    $(".site-header").removeClass("scroll");
		});

		$('.homepage-slider').slick({
			fade: true,
			dots:true,
			adaptiveHeight: true,
			arrows: false,
			// autoplay: true,
			// autoplaySpeed: 2000,
		});

		$('.casestudies-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			dots:false,
			adaptiveHeight: true,
			responsive: [
		    {
		      breakpoint: 1279,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        arrows:true,
		        dots:false,
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        arrows:true,
		        dots:false,
		  		// autoplay: true,
				// autoplaySpeed: 2000,
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});



		//$('.accordion').accordion();

		//$('select').dropdown();

		$(document).on( 'click', '.open-modal', function(e) {
			e.preventDefault();
		    $('.modal').modal('show');
		});



		// Scroll 
		var wrap = $(window);

		wrap.scroll(function() {
		    
		  if ($(window).scrollTop() > 70) {
		  	$(".site-header").addClass("scroll");
		  } else{
		    $(".site-header").removeClass("scroll");
		  }
		  
		});


		//Contact info
		$(document).on( 'click', '.contact-button', function(e) {
			e.preventDefault();
		    $('.contactinfo').toggleClass('active');
		});

		$(".contactinfo").on( 'click', '.close', function(e) {
			e.preventDefault();
		    $('.contactinfo').toggleClass('active');
		});

		$(document).on( 'click', '.contact-button-mobile', function(e) {
			e.preventDefault();
		    $('.contactinfo-mobile').toggleClass('active');
		    $('body').toggleClass('mobile-active');
		    $(".menu-mobile").removeClass("active");
		});

		$(".contactinfo-mobile").on( 'click', '.close', function(e) {
			e.preventDefault();
		    $('.contactinfo-mobile').toggleClass('active');
		    $('body').toggleClass('mobile-active');


		});

		//Case Studies
		$("#casestudies").on( 'click', '.button', function(e) {
			e.preventDefault();
		    $(this).parent().parent().find(".overlay").toggleClass('active');

		    var window_width = $(window).width();
		    console.log(window_width);
		    if(window_width < 1279){
		    	$(this).parent().parent().find(".overlay").appendTo($(".casestudies-overlay").find(".content"));
		    	$(".casestudies-overlay").toggleClass("active");
		    }
		    
		});

		$(".overlay.active").mouseleave(function() {
			  $(this).addClass('active');
		});

		$("#casestudies").on( 'mouseleave', '.item.slick-active', function(e) {
			e.preventDefault();
		    $(this).find(".overlay").removeClass('active');
		});

		$(".casestudies-overlay").on( 'click', '.close', function(e) {
			e.preventDefault();
		    $(".casestudies-overlay").toggleClass("active");
		});




	});

})(jQuery);
