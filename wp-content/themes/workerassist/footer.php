

  <footer class="site-footer">
    <div class="container">
      <div class="column1">
      	<a href="" class="workerassist"><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo-footer.svg"></a><div class="info">
	      	<a href="tel:+1300027747">1300 027 747</a><br>
			<a href="mailto:workerassist@workerassist.org.au">workerassist@workerassist.org.au</a><br>
			379 Elizabeth Street, North Hobart Tasmania 7000<br>
			<a href="#">Privacy Policy</a>
		</div>
      </div>
      <div class="column2">
      	Supported by<br>
      	<a href=""><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo-workcover.png"></a>	
      	<a href=""><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo-union.png"></a>	
      	<a href=""><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo-tasgov.png"></a><br>
      	<p>Authorised by Jessica Munday, Unions Tasmania, 379 Elizabeth St North Hobart Tasmania 7000, 03 6234 9553</p>	
      </div>
      <div class="column3">
      	<a href=""><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo-nac.png"></a>
      </div>
    </div>
  </footer>


<?php wp_footer(); ?>

<div class="print-modal" id="printSection">
 <!--  <header>
    <div class="logo">
      <a href=""><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo.svg"></a>
    </div> 
  </header> -->
  <section class="content"></section>
  <!-- <footer>
    <h5>NEED HELP</h5>
    <p>Contact us for free, confidential and independent advice over the phone or face-to-face. </p>
    <table>
      <tr>
        <td>
          <img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-pencil-blue.svg">
          <h5>ONLINE</h5>
          <a href="workerassist.org.au">workerassist.org.au</a>
        </td>
        <td>
          <img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-phone-blue.svg">
          <h5>CALL US NOW ON<br>1300 027 747</h5>
        </td>
         <td>
          <img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-map-blue.svg">
          <h5>VISIT US</h5>
          <p>379 Elizabeth Street<br>North Hobart</p>
        </td>
      </tr>
    </table>
  </footer> -->
</div>

</body>




</html>
