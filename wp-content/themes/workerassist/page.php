<?php get_header('page');  ?>

<section class="page-segment">
	<div class="container">
		<?php
		while ( have_posts() ) : the_post();
		?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h2><?php the_title(); ?></h2>
				</header>
				<div class="entry-content">
					<?php
						the_content();
						
					?>
				</div>
			</article>
		<?php
		endwhile; // End of the loop.
		?>
	</div>

</section>


<?php get_footer(); ?>
