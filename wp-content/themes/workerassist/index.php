<?php get_header(); ?>

<?php 

$nextstep_image = get_field('homepage_next_step_image', 'option');

$whatwedo_intro_title = get_field('whatwedo_intro_title', 'option');
$whatwedo_intro_text = get_field('whatwedo_intro_text', 'option');


?>

<div class="maincontent">
  
  <!-- HOMEPAGE BANNER -->
  <section class="banner slider">
    <div class="banner-title container">
      <img class="next-step" src="<?php echo $nextstep_image; ?>">
    </div>
    <div class="homepage-slider">

    <?php if( have_rows('homepage_slider', 'option') ): ?>
        <?php while( have_rows('homepage_slider', 'option') ): the_row(); ?>
            <div class="item" style="background:url('<?php the_sub_field('homepage_image'); ?>') no-repeat 50% 50%;"></div>
        <?php endwhile; ?>
    <?php endif; ?>

    </div>
  </section>

  <!-- WHAT WE DO -->
  <section id="whatwedo">
    <div class="container">
      <h4><?php echo $whatwedo_intro_title; ?></h4>  
      <?php echo $whatwedo_intro_text; ?>
    </div>
    <div class="services">
      <div class="container">

        <?php if( have_rows('whatwedo_services', 'option') ): ?>
        <?php while( have_rows('whatwedo_services', 'option') ): the_row(); ?>
        
        <div class="item">
          <div class="image">
            <img src="<?php the_sub_field('service_image'); ?>">
            <div class="whitebar"></div>  
          </div>
          <div class="text">
            <div class="title">
              <div class="icon"><i class="arrow"></i></div>  
              <div class="header-title"><h4><?php the_sub_field('service_title'); ?></h4></div>
            </div>
            <?php the_sub_field('service_description'); ?>
          </div>
        </div>      
  
        <?php endwhile; ?>
        <?php endif; ?>

        
      </div>
    </div>
  </section>

  <!-- CONTACT -->
  <section id="contact">
    <div class="container">
      <div class="column1">
        <h4>NEED HELP?</h4>
        <p>Contact us for free, confidential and independent advice over the phone  or face-to-face.</p>
        <p>Our services are free and available to everyone.</p>
      </div> 
      <div class="column2">
          <div class="call">
            <img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-phone-blue.svg"><br>
            <h4>CALL US NOW ON<br>
            1300 027 747</h4>
          </div>
          <div class="visit">
            <img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-map-blue.svg"><br>
            <h4>VISIT US</h4>
            <h5>379 Elizabeth Street,<br>
            North Hobart</h5>   
          </div>
      </div>
      <div class="column3">
        <img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-pencil-blue.svg"><br>
          <p>Leave your details and we’ll contact you as soon as possible</p>
          <?php gravity_form( 1, false, true, false, '', true ); ?>
      </div>  
    </div>
  </section>


  <!-- CASE STUDIES -->
  <section id="casestudies">
    <div class="container">
      <div class="casestudies-slider">

        <?php
 
        $args = array(
            post_type => array('casestudies'),
        );
         
        // Custom query.
        $query = new WP_Query( $args );
         
        // Check that we have query results.
        if ( $query->have_posts() ) {
         
            // Start looping over the query results.
            while ( $query->have_posts() ) {
         
                $query->the_post();

                $casestudies_image = get_field('casestudies_image');
                $casestudies_name = get_field('casestudies_name');
                $casestudies_textintro = get_field('casestudies_textintro');
                $casestudies_solutions = get_field('casestudies_solutions');
               ?>

              <div class="item">
                <div class="image">
                  <img src="<?php echo $casestudies_image; ?>">
                  <div class="whitebar"></div>  
                </div>
                <div class="text">
                  <div class="title">
                    <h5>CASE STUDY | <span><?php echo $casestudies_name; ?></span></h5>
                  </div>
                  <?php echo $casestudies_textintro; ?>
                  <a href="" class="button">How we helped <?php echo $casestudies_name; ?></a>
                </div>
                <div class="overlay">
                  <div class="whitebar"></div>
                  <h5>HOW WE HELPED | <span><?php echo $casestudies_name; ?></span></h5>
                  <?php echo $casestudies_solutions; ?>
                </div>
              </div>

               <?php
            }
         
        }
         
        // Restore original post data.
        wp_reset_postdata();
         
        ?>

        
        

       


    </div>  
  </section>

  <!-- NEWS & EVENTS -->
  <section id="newsevents">
    <div class="container">
      <h4>NEWS AND EVENTS</h4>

        <?php
 
        $args = array(
          'post_type' => array('post'),
          'posts_per_page' => 5,
        );

        $args2 = array(
          'post_type' => array('post'),
        );
         
        // Custom query.
        $query = new WP_Query( $args );
        $query2 = new WP_Query( $args2);
        $count = $query2->post_count;
         
        // Check that we have query results.
        if ( $query->have_posts() ) {
         
            // Start looping over the query results.
            while ( $query->have_posts() ) {
         
                $query->the_post();
         
               ?>
                
                <div class="item">
                  <div class="arrow"></div>  
                  <div class="news">
                    <h5><?php the_title(); ?></h5>
                    <?php the_excerpt(); ?>
                    <?php 
                    $thecontent = get_the_content();
                    if(!empty($thecontent)) { 
                    ?>
                    <a href="<?php the_permalink(); ?>">Read more »</a>
                    <?php } ?>
                  </div>
                </div>
         

               <?php
            }
         
        }

        if($count > 5){
          echo "<a href='news-events' class='button viewall'>VIEW ALL</a>";
        }
         
        ?>
        
        <?php 
        wp_reset_postdata();
        ?>


    </div>    
      
  </section>

</div>


<?php get_footer();
