<?php get_header(); ?>

<?php 

$nextstep_image = get_field('resource_next_step_image', 'option');

$resource_banner_image = get_field('resource_banner_image', 'option');

?>

<div class="maincontent">
	<section class="banner subpage">
	    <div class="banner-title container">
	      <img class="next-step" src="<?php echo $nextstep_image; ?>">
	    </div>
		<div class="overlay" style="background:url('<?php echo $resource_banner_image; ?>') no-repeat 50% 0;"></div>
	</section>

	<div class="container resources">
		<div class="left-column">
			<h4>RESOURCES</h4>
			<p>If you’ve been injured at work, these documents and links could help you take the next step.  For free, independent and confidential advice, contact us today.</p>
			<hr>	
			<div class="resources-list">


				<?php
 
				$args = array(
				    'post_type' => 'resources',
				);
				 
				// Custom query.
				$query = new WP_Query( $args );
				 
				// Check that we have query results.
				if ( $query->have_posts() ) {
				 
				    // Start looping over the query results.
				    while ( $query->have_posts() ) {
				        $query->the_post();
				        $post_id = get_the_ID();
				        $print_count =  get_field('print_count');
				       ?>

				       	<div class="item" id="r<?php echo $post_id; ?>">
							<h5 class="entry-title"><?php the_title(); ?></h5>
							<?php the_content(); ?>
							<a href="<?php echo get_bloginfo( 'url' )?>/print-template/?postid=<?php echo $post_id; ?>" class="button print" target="_new">Print this</a>
							<input type="hidden" id="printcount" value="<?php echo $print_count;  ?>">
							<input type="hidden" id="postid" value="<?php echo $post_id; ?>">

						
						</div>

				       <?php
				    }
				}
				 
				// Restore original post data.
				wp_reset_postdata();
				 
				?>


			</div>
		</div>

		<div class="right-column">
			<div class="widget">
				<h4 class="title">WHAT WOULD YOU LIKE TO KNOW?</h4>
				<ul>

					<?php
 
					$args = array(
					    'post_type' => 'resources',
					);
					 
					// Custom query.
					$query = new WP_Query( $args );
					 
					// Check that we have query results.
					if ( $query->have_posts() ) {
					 
					    // Start looping over the query results.
					    while ( $query->have_posts() ) {
					        $query->the_post();
					        $post_id = get_the_ID();
					       ?>

					       	<li><a class="resource-link" href="#r<?php echo $post_id; ?>"><i class="arrow"></i> <span><?php the_title(); ?></span></a></li>

					       <?php
					    }
					}
					 
					wp_reset_postdata();
					 
					?>

				
				</ul>
			</div>

			<div class="widget">
				<h4 class="title">USEFUL LINKS</h4>
				<ul>
					<?php if( have_rows('links', 'option') ): ?>
				        <?php while( have_rows('links', 'option') ): the_row(); ?>
				            <li><a href="<?php the_sub_field('link_url'); ?>"><i class="arrow"></i> <span><?php the_sub_field('link_title'); ?></span></a></li>
				        <?php endwhile; ?>
				    <?php endif; ?>
				</ul>
			</div>
		</div>
	</div>

	<input type="hidden" id="printcount" value="">
	

</div>




<?php get_footer(); ?>





