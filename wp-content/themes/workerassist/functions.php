<?php
/**
 * starter functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package starter
 */

if ( ! function_exists( 'starter_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function starter_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on starter, use a find and replace
	 * to change 'starter' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'starter', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );


	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'starter' ),
		'footer' => esc_html__( 'Footer', 'starter' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'starter_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'starter_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function starter_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'starter_content_width', 640 );
}
add_action( 'after_setup_theme', 'starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function starter_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'starter' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'starter_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function starter_scripts() {
	//wp_enqueue_style( 'starter-style', get_stylesheet_uri() );

	//wp_enqueue_script( 'starter-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	//wp_enqueue_script( 'starter-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	//wp_deregister_script( 'jquery' );
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );

	
	wp_enqueue_style( 'style', get_template_directory_uri() . "/assets/css/style.css", array(), '1.0', 'screen' );

	//wp_enqueue_style( 'print', get_template_directory_uri() . "/assets/css/print.css", array(), '1.0', 'print' );


	//wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.1.1.min.js', array( 'jquery' ), '3.1.1', true );
	wp_enqueue_script( 'debouncedresize', get_template_directory_uri() . '/assets/js/jquery.debouncedresize.js', array( 'jquery' ), '1.12.0', true );
	
	wp_enqueue_script( 'accordion', get_template_directory_uri() . '/assets/js/plugins/accordion.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'checkbox', get_template_directory_uri() . '/assets/js/plugins/checkbox.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'transition', get_template_directory_uri() . '/assets/js/plugins/transition.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'dropdown', get_template_directory_uri() . '/assets/js/plugins/dropdown.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'modal', get_template_directory_uri() . '/assets/js/plugins/modal.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'dimmer', get_template_directory_uri() . '/assets/js/plugins/dimmer.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/plugins/slick.min.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/assets/js/plugins/smooth-scroll.min.js', array( 'jquery' ), '1.12.0', true );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'ajax-printcount', get_template_directory_uri() . '/assets/js/printupdate.js' );
	
	$vars = array( 'ajax_url' => admin_url( 'admin-ajax.php' ) );

	wp_localize_script( 'ajax-printcount', 'AJAX_PRINT_COUNT', $vars );

	

}
add_action( 'wp_enqueue_scripts', 'starter_scripts' );


/**
 * AJAX PRINT COUNT
 */
add_action( 'wp_ajax_nopriv_starter_print_count', 'print_count' );
add_action( 'wp_ajax_starter_print_count', 'print_count' );

function print_count() {

	//ob_start();

	$postid  = isset( $_POST['postid'] ) ? absint( $_POST['postid'] ) : '';

	
// 	// get current count value
$count = (int)get_field('print_count', $postid);


// increase
 $count++;


	// update
update_field('print_count', $count , $postid);

	die();
	
}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom Posts Type
 */
require get_template_directory() . '/inc/custom-posts-type.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



/**
 * Advanced Custom Fields (Theme Settings)
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'What we do',
		'menu_title'	=> 'What we do',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact Info',
		'menu_title'	=> 'Contact Info',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Useful Links',
		'menu_title'	=> 'Useful Links',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}




// filter the Gravity Forms button type
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}



