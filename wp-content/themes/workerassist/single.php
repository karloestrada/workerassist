


<?php get_header(); ?>

<div class="maincontent">
	<section class="banner subpage">
		<div class="overlay" style="background:url('http://placehold.it/2350x518') no-repeat 50% 0;"></div>
	</section>

	<div class="container singlepage">
		<div class="left-column">

		<?php while ( have_posts() ) : the_post(); ?>

			<h4 class="entry-title"><?php the_title(); ?></h4>
			<?php the_content(); ?>

		<?php	
		endwhile;
		?>
				
			
		</div>

		<div class="right-column">
			
			<a href="#" class="button printnews">Print this</a>

			<br><br>

			<div class="widget">
				<h4 class="title">LATEST NEWS</h4>
				<ul>

					<?php
 
			        $args = array(
			          'post_type' => array('post'),
			          'posts_per_page' => 8,
			        );
			         
			        // Custom query.
			        $query = new WP_Query( $args );
			         
			        // Check that we have query results.
			        if ( $query->have_posts() ) {
			         
			            // Start looping over the query results.
			            while ( $query->have_posts() ) {
			         
			                $query->the_post();
			         
			               ?>
			                
			                <li><a href="<?php echo get_permalink(); ?>"><i class="arrow"></i> <span><?php the_title(); ?></span></a></li>
			         

			               <?php
			            }
			         
			        }
			         
			        // Restore original post data.
			        wp_reset_postdata();
			         
			        ?>


					
				</ul>
			</div>
			
		</div>
	</div>
	

</div>




<?php get_footer(); ?>





