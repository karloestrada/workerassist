<?php 

/**
 * Template Name: Print Template
 *
 * @package Starter
 */

?>


<?php get_header("print"); ?>




<section class="content">

<div class="container">

<?php
 
$args = array(
'post_type' => array('resources','post','pages','casestudies'),
  'p' => $_GET['postid'],
);
 
// Custom query.
$query = new WP_Query( $args );
 
// Check that we have query results.
if ( $query->have_posts() ) {
 
    // Start looping over the query results.
    while ( $query->have_posts() ) {
 
        $query->the_post();
 
       ?>

		<h4 class="entry-title"><?php the_title(); ?></h4>
		<?php the_content(); ?>

       <?php
 
    }
 
}
 
// Restore original post data.
wp_reset_postdata();
 
?>
</div>


</section>

<?php get_footer(); ?>





