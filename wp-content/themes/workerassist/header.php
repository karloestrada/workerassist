<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=100%, initial-scale=1.0, 
minimum-scale=1.0, maximum-scale=1.0, user-scalable=false">
	<meta name="HandheldFriendly" content="true">
	<meta name="theme-color" content="#ffffff">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/manifest.json">
	<link rel="mask-icon" href="<?php echo get_bloginfo( 'template_directory' )?>/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
	

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
	
	<script>
		var HOME_DIR = '<?php echo get_bloginfo( 'url' )?>';
		var THEME_DIR = '<?php echo get_bloginfo( 'template_directory' )?>'
	</script>

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?> id="body">

	<div class="casestudies-overlay">
		<div class="close"></div>
		<div class="content"></div>
	</div>

	<div class="menu-mobile">
		<div class="menu">
	      <a href="#contact" class="item" id="mobile-contact">Contact</a>
	      <a href="#whatwedo" class="item" id="mobile-whatwedo">What we do</a>
	      <a href="#casestudies" class="item" id="mobile-casestudies">Case Studies</a>  
	      <a href="#newsevents" class="item" id="mobile-news">News & Events</a>
	      <a href="<?php echo get_bloginfo( 'url' )?>/resources" class="item not-anchor" id="mobile-resources">Resources</a>
    	</div>
	</div>
	<div class="contact-button-mobile">
	</div>
	<div class="contactinfo-mobile">
		<div class="close">
		</div>
		<img class="title" src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/gethelpnow2.svg">	
		<div class="call">
			<img class="icon" src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-phone.svg"><a href="tel:+1300 027 747">1300 027 747</a>
		</div>
		<div class="visit">
			<img class="icon" src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-map.svg">379 Elizabeth Street, North Hobart
		</div>
		<div class="email">
			Leave your details and we’ll call you as soon as possible.

			<?php gravity_form( 1, false, true, false, '', true ); ?>
		</div>	
		
	</div>


	<header class="site-header">
		<div class="container">
			<div class="menu-toggle">
				<a href="" class="ion-navicon"></a>
			</div>
			<div class="logo">
				<a href="<?php echo get_bloginfo( 'url' )?>"><img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/logo.svg"></a>
			</div>  
			<div class="main menu">
				<a href="#whatwedo" class="active item">What we do</a>
				<a href="#newsevents" class="item">News & Events</a>
				<a href="#casestudies" class="item">Case Studies</a>  
				<a href="<?php echo get_bloginfo( 'url' )?>/resources" class="item not-anchor">Resources</a>
				<a href="#contact" class="item">Contact Us</a>
			</div>
			<div class="contact-button">
				<img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/gethelpnow.svg">
			</div>
			<div class="contactinfo">
				<div class="close">
				</div>
				<div class="column1">
					<div class="call">
						<img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-phone.svg"><br>
						CALL US NOW ON<br>
						1300 027 747
					</div>
					<div class="visit">
						<img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-map.svg"><br>
						VISIT US <br>
						<span>379 Elizabeth Street,
						North Hobart</span> 	
					</div>
				</div>
				<div class="column2">
					<img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/icon-pencil.svg"><br>
					<p>Leave your details and we’ll contact you as soon as possible</p>
					<?php gravity_form( 1, false, true, false, '', true ); ?>
				</div>	
				<div class="column3">
					<img src="<?php echo get_bloginfo( 'template_directory' )?>/assets/images/gethelpnow2.svg">	
				</div>
			</div>
			
		</div>
	</header>




