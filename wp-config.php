<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'workerassist');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ykd~kDGR6V?V3NmDnBDG`fz7EXq 8{RA+deuoOt|tk~i574h.Z}v!(<.E[$|h (r');
define('SECURE_AUTH_KEY',  'P*gBGDX9(ht-C}`b7nWS*ryEA`BgNVmSeHs{0x|>_d`PGpva7.WnJ>gW:z)HVsEO');
define('LOGGED_IN_KEY',    'f7W89J4Yelkgh3-zHXJdg/6tY<m$R}?*z^5R1r&&@?H|G/&:)jZTdsZq{%BZV`2g');
define('NONCE_KEY',        'x|zFJ>!9b,GODP0<D^|QP,>UXRw|0=Wm)z3LPruCseHuIVq=f.]rO0BNL2K-A;DM');
define('AUTH_SALT',        'e`miHXw{]r-j)146m8hh/dzyTFvI{J^9_V4(f4lvSpnfG7LA$6 m-xssbT!*(;;|');
define('SECURE_AUTH_SALT', '?y27T;t(.EFRm&fssqR@h%GDZoWa(zKpNx!G;UtF%-2i(B@[u^_lAOzV1l 0h~~D');
define('LOGGED_IN_SALT',   'NHn[i[iLtR5kTe;8JQNhO~&,Wbif>:qF-kCNORwQAum5H]N:o>#2!nqr9M4I4F;t');
define('NONCE_SALT',       ']$Hv>YMv,|IQh5Nsl&2 u%!6l)ft:j?et=r#+~1Z4FJDP(pM*vHGt%/*x>]Mc7Cf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
